<?php
class Author extends TParser{
    public static $slug;
    public static $page;
    public static $hadpost;

    public static function getHotPost(){
        Posts::$args = array( 
            'posts_per_page' => 2,
            'orderby' => 'post_views',
            'date_query' => array(
                //array('after' => '12 weeks ago',),
            ),
            'tax_query' => array(
                array(
                    'taxonomy' => 'butdanh',
                    'field' => 'slug',
                    'terms' => static::$slug
                ),
            ),
            'suppress_filters' => false
        );
        return Posts::getPostsByFilter(true);
    }

    public static function getNewPost(){
        Posts::$args = array( 
            'posts_per_page' => 10,
            'post__not_in' => static::$hadpost,
            'offset' => static::$page * 10,
            'tax_query' => array(
                array(
                    'taxonomy' => 'butdanh',
                    'field' => 'slug',
                    'terms' => static::$slug
                ),
            ),
            'suppress_filters' => false
        );
        return Posts::getPostsByFilter();
    }
}