<?php
function search_by_title( $search, $wp_query ) {
	if ( ! empty( $search ) && ! empty( $wp_query->query_vars['search_terms'] ) ) {
		global $wpdb;
		
		$q = $wp_query->query_vars;
		$n = '%'; // search không dấu
		
		$search = array();
		foreach ( ( array ) $q['search_terms'] as $term )
			$search[] = $wpdb->prepare( "$wpdb->posts.post_title LIKE %s", $n . $wpdb->esc_like( $term ) . $n );
			if ( ! is_user_logged_in() )
				$search[] = "$wpdb->posts.post_password = ''";
				$search = ' AND ' . implode( ' AND ', $search );
	}
	return $search;
}
add_filter( 'posts_search', 'search_by_title', 10, 2 );


// function progessive_images_html($attachment_id, $size, $ratio){
//     $image = wp_get_attachment_image($attachment_id, $size);
//     $html = sprintf('
//     <span class="img-holder" >
//         <span class="img-ratio-fill" style="padding-bottom: %f%%;"></span>
//         <span class="img-progressive" >
//             %s
//         </span>
//     </span>',
//     $ratio,
//     $image
//     );

//     return $html;
// }

add_filter('the_content', 'lb_make_img_fix', 21);

// function lb_make_img_fix( $content ) {
//     if ( ! preg_match_all( '/<figure\W*id="attachment_(\d+)"\W*class="([^">]*)"[^>]*>\W*(<img\W*class="([^">]*)"[^>]*\/>)(.*)<\/figure>/isU', $content, $matches ) ) {
//         return $content;
//     }
//     $selected_images = $attachment_ids = array();
//     foreach( $matches[0] as $i => $figure ) {
//       $org_figure = $figure;
//       $image = $matches[3][$i];
//       $attachment_id = $matches[1][$i];
//       $image_class = $matches[4][$i];
//       $figure_class = $matches[2][$i];

//       preg_match('/size-([^"|\s>]*)/', $image_class, $size_matches);

//       $size = isset($size_matches[1]) ? $size_matches[1] : 'img-normal';

//       $image_meta = wp_get_attachment_metadata( $attachment_id );

//       $figure = str_replace($matches[2][$i], $figure_class.' '.$image_class, $figure);

//       $ratio = $image_meta['height']*100/$image_meta['width'];

//       $progessive_image = progessive_images_html($attachment_id, $size, $ratio);
//       if($progessive_image){
//           $figure = str_replace( $image, $progessive_image, $figure );
//       }

//       $content = str_replace( $org_figure, $figure, $content );
//     }
//     return $content;
// }