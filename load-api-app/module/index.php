<?php
class Index extends TParser{
    public static $posts_per_page = 5;
    public static $cate;

    public static function featured(){
        $zone_size = 5;
        $featured_ids = [];

        $backup_posts_query = new WP_Query(array(
            'meta_key'  => 'boombox_is_featured',
            'meta_value' => 1,
            'posts_per_page' => $zone_size
        ));

        $backup_ids = wp_list_pluck( $backup_posts_query->posts, 'ID' );
        $featured_posts = get_field('posts', FEATURED_ZONE);
        $now = current_time('timestamp');
        for ($i=0; $i < $zone_size; $i++) {
            if($featured_posts){
                $row = $featured_posts[$i];
                if($row['post']){
                    $start = empty($row['start']) ? true : strtotime($row['start']) <= $now;
                    $end = empty($row['end']) ? true : strtotime($row['end']) >= $now;
                    if(get_post_status($row['post']) == 'publish' && $start && $end){
                        $featured_ids[] = $row['post'];
                    }
                    // auto clear bai qua han
                    if(!empty($row['end']) && strtotime($row['end']) < $now){
                        $result = update_row('posts', $i+1, ['post' => false, 'start' => false, 'end' => false], FEATURED_ZONE);
                    }

                }
            }

            if(count($featured_ids) < $i + 1){
                $id_to_add = 0;
                do{
                    $id_to_add = array_shift($backup_ids);
                } while(in_array($id_to_add, $featured_ids));
                $featured_ids[] = $id_to_add;
            }
        }
        Posts::$args =  array(
            'post_type' => 'post',
            'post__in'  => $featured_ids,
            'orderby'	=> 'post__in'
        );
        return Posts::getPostsByFilter(true);
    }

    public static function newest( $hadPost = true){
        Posts::$args =  array(
            'post_type' => 'post',
            'posts_per_page'  => self::$posts_per_page,
            'post__not_in' => Posts::$hadPost,
        );
        return Posts::getPostsByFilter($hadPost);
    }

    public static function trending(){
        global $wpdb;
        $term_ids = $wpdb->get_col("
            SELECT  term_id,
                count(*) cnt
            FROM $wpdb->term_taxonomy
            INNER JOIN $wpdb->term_relationships
                ON $wpdb->term_taxonomy.term_taxonomy_id=$wpdb->term_relationships.term_taxonomy_id
                AND $wpdb->term_taxonomy.taxonomy='post_tag'
            INNER JOIN
                $wpdb->posts
                ON $wpdb->posts.ID = $wpdb->term_relationships.object_id
            WHERE DATE_SUB(CURDATE(), INTERVAL 3 DAY) <= $wpdb->posts.post_date
            AND $wpdb->posts.post_status = 'publish'
            GROUP BY term_id
            ORDER BY cnt DESC
            LIMIT 5
        ");

        Posts::$args = array(
            'posts_per_page' => self::$posts_per_page,
            'post__not_in' => Posts::$hadPost,
            'order' => 'desc',
            'post_type' => 'post',
            'tag__in' => $term_ids
        );
        return Posts::getPostsByFilter(true);
    }

    public static function categoryList(){
        Posts::$args = array(
            'post_type' => 'post',
            'posts_per_page'  => self::$posts_per_page,
            'post__not_in' => Posts::$hadPost,
            'category__in' => $cate
        );
        return Posts::getPostsByFilter(true);
    }
}