<?php
class TParser{
	public static $post;
	public static $term;
	public static $format;
	
	/** Get field ACF */
	public static function getf($name, $flag = false){
		return get_field($name, static::$post, $flag);
	}
	
	/** Get field ACF option */
	public static function geto($name, $flag = false){
		return get_field($name, 'option', $flag);
	}
	
	/** Get field ACF taxonomy */
	public static function gett($name){
		return get_term_meta(self::$term, $name);
	} 
	
	/** Get image from image ID */
	public static function get_image($id){
		$image = wp_get_attachment_image_src($id, 'full');
		return ($image)? $image : array();
    }
    
    public static function stream_domain($domain){
        $location = preg_replace('/^((http(s)?:\/\/)?([^\/]+)(\/)?)(.*)/', '$1', $domain);
        return str_replace($location, '', $domain);   
    }
	
	/**
	 * @param $data : array(key1 => value1, key2 => value2, etc...)
	 * @return void|mixed
	 */
	public static function parser($data){
		if(empty($data)) return;
		$format = static::$format;
		foreach($data as $key => $value)
			$format= str_replace('{{'.$key.'}}', $value, $format);
		return $format;
	}
	
	/**
	 * @param $data : array(value1, value2, value3, etc...)
	 * @return void|string|mixed
	 */
	public static function parserOne($data){
		if(empty($data)) return;
		$result = '';
		$format = static::$format;
		foreach($data as $value) $result .= str_replace('{{keyname}}', $value, $format);
		return $result;
	}
	
	/**
	 * @param $source : array(
	 * 						array(key1 => value1, key2 => value2), 
	 * 						array(key1 => value3, key2 => value4), 
	 * 						etc...
	 * 					)
	 * @param $count : How many row do you want to display
	 * @return string|void|mixed
	 */
	public static function parserArr($source, $count = -1){
		$result = '';
		$step	= 0;
		foreach ($source as $data) {
			if($step === $count) 
				break;
			$result .= self::parser($data); $step++;
		}
		return $result;
	}
}
