<?php
class Posts extends TParser{
    public static $NSFW_CATEGORY = 2;
	public static $args;
    public static $post;
    public static $term;
	public static $hadPost = array();
	public static $countPost;
	public static $maxPage;
	public static $format;
	public static $date = 'H:i d-m-Y';
	
	public static function setHadPost($post){
		self::$hadPost[] = $post;
	}
	
    public static function getPostsByFilter($hasPost = false){
        global $post,$prefix;
        $result 	= array();
        $args       = array('post_type' => 'post');
        $the_query  = new WP_Query(static::$args); 
        
        self::$countPost 	= $the_query->found_posts;
        self::$maxPage 	    = $the_query->max_num_pages;
        
        if ( $the_query->have_posts() ){ $posts = $the_query->posts;
        	foreach($posts as $post):
            	if($hasPost) self::setHadPost($post->ID);
            	
                Posts::$post 	= $post->ID;
                Terms::$post    = $post->ID;

            	$obj['id']      = $post->ID;
            	$obj['title']	= Terms::get_post_format_icon().' '.$post->post_title; 
            	$obj['cate']    = Terms::get_primary_cat();
                $obj['image']   = get_img_from_postID(static::$post, null, null);
                $obj['skull']   = has_category(static::$NSFW_CATEGORY, $post->ID);
                $obj['link']    = '/'.TParser::stream_domain(get_permalink());
                $obj['excerpt'] = get_the_excerpt();
                $obj['view']    = $post->post_views; 
                $obj['date']	= get_the_date('d-m-Y');
                $obj['daytotime'] = strtotime(get_the_date('d-m-Y'));
                $obj['author']	= self::get_author();
				$obj['series']	= get_post_meta($post->ID, 'series', true);
               
                $result[]       = $obj;
			endforeach;
        }
        wp_reset_query();
        return $result;
    } 

    // public static function getSeoInfo(){
    //     echo get_post_meta( 318217, '_yoast_wpseo_title', true );
    //     echo get_post_meta( 318217, '_yoast_wpseo_metadesc', true);
    // }
    
    public static function getPosts(){
    	$result = array();
    	while ( have_posts() ) : the_post();
	    	$obj			= self::getSinglePost();
	    	$obj['image']	= get_img_from_postID(static::$post, 300, 300);
	    	$obj['link']   	= get_permalink();
	    	$obj['excerpt'] = apply_filters( 'the_content', get_the_excerpt(30));
	    	$result[]       = $obj;
    	endwhile;
    	return $result;
    }
    
    public static function getSinglePost(){
        $singlePost         = get_post(Posts::$post);  self::setHadPost(Posts::$post);  
        Terms::$post		= Posts::$post;
        
        $obj['id']		    = Posts::$post;
        $obj['title']		= $singlePost->post_title;
        $obj['date']		= $singlePost->post_date;
        $obj['image']       = get_img_from_postID(static::$post, null, null);
        $obj['cate']    	= Terms::get_primary_cat();
        $obj['source']      = self::getf('nguon');       
        $obj['content'] 	= apply_filters('the_content', $singlePost->post_content);
        $obj['excerpt'] 	= $singlePost->post_excerpt;
        $obj['author']	    = self::get_author();
        $obj['tags']        = wp_get_post_tags(Posts::$post);
        $obj['more']        = self::getListFromPostID(self::getf('xem_them'));
        $obj['good']        = self::getListFromPostID(self::getf('hay_ne'));

        $series = self::getSeries($obj['id']);
        $obj['serie']       = ($series !== null)? $tax_id = (int)get_the_terms($postID, 'serie')[0]->term_id : null;
    
        $obj['hadpost']     = self::$hadPost;
    	return $obj;
    }

    public static function getSeries($postID){
        $tax_id = (int)get_the_terms($postID, 'serie')[0]->term_id;
        if($tax_id === 0) return null;

        static::$args = array(
            'posts_per_page' => -1,
            'post_type' => 'post',
            'orderby' => 'id',
            'order' => 'ASC',
            'tax_query' => array(
                array(
                    'taxonomy' => 'serie',
                    'field' => 'term_id',
                    'terms' => $tax_id,
                )
            )
        ); 
        return self::getPostsByFilter();
    }

	public static function bubleSort($obj, $str){
		for($i = 0; $i < count($obj) - 1; $i++){
			if( (int)$obj[$i][$str] > (int)$obj[$i+1][$str]){
				$temp = $obj[$i];
				$obj[$i] = $obj[$i + 1];
				$obj[$i + 1] = $temp;
			}
		}
		return $obj;
	}
	
    public static function getPostsTags(){
        $tag_ids = wp_get_post_tags( static::$post, array( 'fields' => 'ids' ) );
        static::$args = array(
            'post__not_in' => self::$hadPost,
            'posts_per_page' => 6,
            'tax_query' => array(
                array(
                    'taxonomy' => 'post_tag',
                    'field' => 'term_id',
                    'terms' => $tag_ids,
                ),
            ),
        );
        return self::getPostsByFilter( true );
    }

    public static function getPostsSimilar(){
        static::$args = array(
            'post__not_in' => self::$hadPost,
            'category__in' => array(self::$term),
            'posts_per_page' => 10
        );
        return self::getPostsByFilter( true );
    }

    public static function getListFromPostID($postid){
        if($postid == null) return;
        $result = array();
        if(is_array($postid)){
            foreach( $postid as $id){
                $obj = array();
                $obj['link']    = '/'.TParser::stream_domain(get_permalink($id));
                $obj['title']   = get_the_title($id);
                $obj['skull']   = has_category(static::$NSFW_CATEGORY, $id);
                $obj['image']   = get_img_from_postID($id, null, null);
                $result[]       = $obj;
            }
        }else{
            $obj['link']        = '/'.TParser::stream_domain(get_permalink($postid));
            $obj['title']       = get_the_title($postid);
            $obj['skull']       = has_category(static::$NSFW_CATEGORY, $postid);
            $obj['image']       = get_img_from_postID($postid, null, null);
            $result[]           = $obj;
        }
        return $result;
    }
    
    public static function get_author(){
        $term = get_term( self::getf('butdanh'));
        TParser::$term      = $term->term_id;
        $term->avatar       = get_img_from_ID(self::gett('avatar')[0]);
    	return $term;
    }
	
	public static function getSingleSchemaPost(){
        Terms::$post		= self::$post;
        Terms::get_primary_cat();
        
        $singlePost         = get_post(self::$post);
    	
    	$thumb				= get_img_from_postID(self::$post);
    	
    	$obj['title']		= $singlePost->post_title;
        $obj['date']		= $singlePost->post_date;
    	$obj['dateModify']	= $singlePost->post_modified;
    	$obj['link']    	= get_permalink($singlePost->ID);
    	$obj['archive'] 	= get_cat_name(Terms::$term);
    	$obj['datePublic']	= get_the_date();
    	$obj['excerpt'] 	= apply_filters( 'the_content', get_the_excerpt(30));
    	
    	return $obj;
    }
}
