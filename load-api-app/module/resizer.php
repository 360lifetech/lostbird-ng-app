<?php
function aq_resize( $url, $width, $height = null, $crop = null, $single = true ) {
	if(!$url OR !$width ) return false;

	//define upload path & dir
	$upload_info = wp_upload_dir(); 
	$upload_dir = $upload_info['basedir'];
	$upload_url = $upload_info['baseurl']; 

	//check if $img_url is local
	if(strpos( $url, $upload_url ) === false) return false;

	//define path of image
	$rel_path = str_replace( $upload_url, '', $url);
	$img_path = $upload_dir . $rel_path;

	//check if img path exists, and is an image indeed
	if( !file_exists($img_path) OR !getimagesize($img_path) ) return false;

	//get image info
	$info = pathinfo($img_path);
	$ext = $info['extension'];
	list($orig_w,$orig_h) = getimagesize($img_path);

	//get image size after cropping
	$dims = image_resize_dimensions($orig_w, $orig_h, $width, $height, $crop);
	$dst_w = $dims[4];
	$dst_h = $dims[5];

	//use this to check if cropped image already exists, so we can return that instead
	$suffix = "{$dst_w}x{$dst_h}";
	$dst_rel_path = str_replace( '.'.$ext, '', $rel_path);
	$destfilename = "{$upload_dir}{$dst_rel_path}-{$suffix}.{$ext}";

	if(!$dst_h) {
		//can't resize, so return original url
		$img_url = $url;
		$dst_w = $orig_w;
		$dst_h = $orig_h;
	}
	//else check if cache exists
	elseif(file_exists($destfilename) && getimagesize($destfilename)) {
		$img_url = "{$upload_url}{$dst_rel_path}-{$suffix}.{$ext}";
	}
	//else, we resize the image and return the new resized image url
	else {
		// Note: This pre-3.5 fallback check will edited out in subsequent version
		if(function_exists('wp_get_image_editor')) {
			$editor = wp_get_image_editor($img_path);
			if ( is_wp_error( $editor ) || is_wp_error( $editor->resize( $width, $height, $crop ) ) )
				return false;
			$resized_file = $editor->save();
			if(!is_wp_error($resized_file)) {
				$resized_rel_path = str_replace( $upload_dir, '', $resized_file['path']);
				$img_url = $upload_url . $resized_rel_path;
			} else {
				return false;
			}
		} else {
			$resized_img_path = image_resize( $img_path, $width, $height, $crop ); // Fallback foo
			if(!is_wp_error($resized_img_path)) {
				$resized_rel_path = str_replace( $upload_dir, '', $resized_img_path);
				$img_url = $upload_url . $resized_rel_path;
			} else {
				return false;
			}
		}
	}
	//return the output
	if($single) {
		//str return
		$image = $img_url;
	} else {
		//array return
		$image = array (
			0 => $img_url,
			1 => $dst_w,
			2 => $dst_h
		);
	}
	return $image;
}

/*** GET IMAGE - 1.from post ID - 2. from img ID ***/

if (!function_exists('get_img_from_postID')) {
	function get_img_from_postID($post_id = NULL, $w = NULL, $h = NULL) {
		if (!has_post_thumbnail($post_id)) 
			return '';
		$thumb = get_post_thumbnail_id($post_id); 
		return get_img_from_ID($thumb, $w, $h);
	}
}

if (!function_exists('get_img_from_ID')) {
	function get_img_from_ID($id_img = NULL, $w = NULL, $h = NULL) {
		$img_url 	= wp_get_attachment_url($id_img, 'full'); 
		$size		= wp_get_attachment_metadata($id_img); 
		$img_info 	= get_post( $id_img ); 
		if($w == $size['width'] || $w == null):
			$h 		= $size['height'];  
		else:
			$image 	= aq_resize($img_url, $w, $h, true, false);
            $img_url= $image[0];
            $h      = $image[2];
        endif;
        if (empty($img_url)) return '';
        return $img_url;
	}
}