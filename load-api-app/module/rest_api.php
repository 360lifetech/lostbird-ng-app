<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define( 'APIV', 'lostbird/v1/' );

add_action( 'rest_api_init', 'dt_register_api_hooks' );
function dt_register_api_hooks() {

	register_rest_route( APIV, '/single/(?P<postid>[0-9]*)', array(
		'methods' => 'GET',
		'callback' => 'get_single_by_id',
    ));

    register_rest_route( APIV, '/serie/(?P<postid>[0-9]*)', array(
		'methods' => 'GET',
		'callback' => 'get_serie_by_id',
    ));

    register_rest_route( APIV, '/postsuggest/(?P<post>[0-9]+(?:-[0-9]+)*)/(?P<cate>[0-9]+(?:-[0-9]+)*)', array(
        'methods' => 'GET',
        'callback' => 'get_post_suggest',
        'args' => array(
            'post' => array (
                'required' => true
            ),
            'cate' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/randompost/(?P<post>[0-9]+(?:-[0-9]+)*)', array(
        'methods' => 'GET',
        'callback' => 'get_post_random',
        'args' => array(
            'post' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/poststags/(?P<hadpost>[0-9]+(?:-[0-9]+)*)/(?P<postid>[0-9]*)', array(
        'methods' => 'GET',
        'callback' => 'get_post_tags',
        'args' => array(
            'hadpost' => array (
                'required' => true
            ),
            'postid' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/related/(?P<hadpost>[0-9]+(?:-[0-9]+)*)/(?P<catid>[0-9]*)', array(
        'methods' => 'GET',
        'callback' => 'get_post_related',
        'args' => array(
            'hadpost' => array (
                'required' => true
            ),
            'catid' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/more/(?P<hadpost>[0-9]+(?:-[0-9]+)*)/(?P<page>[0-9]{1,2})', array(
        'methods' => 'GET',
        'callback' => 'get_more_post',
        'args' => array(
            'hadpost' => array (
                'required' => true
            ),
            'page' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/taxonomy/(?P<term>[a-zA-Z_]+)/(?P<slug>[a-zA-Z0-9-]+)', array(
		'methods' => 'GET',
        'callback' => 'get_taxonomy_by_slug',
        'args' => array(
            'term' => array (
                'required' => true
            ),
            'slug' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/author/(?P<slug>[a-zA-Z0-9-]+)/(?P<page>[0-9]{1,2})/(?P<hadpost>[0-9]+(?:-[0-9]+)*)', array(
        'methods' => 'GET',
        'callback' => 'get_author_post',
        'args' => array(
            'slug' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/archive/(?P<slug>[a-zA-Z0-9-]+)/(?P<page>[0-9]{1,2})', array(
		'methods' => 'GET',
        'callback' => 'get_archive_by_slug',
        'args' => array(
            'slug' => array (
                'required' => true
            ),
            'page' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/tags/(?P<slug>[a-zA-Z0-9-]+)/(?P<page>[0-9]{1,2})', array(
		'methods' => 'GET',
        'callback' => 'get_tags_by_slug',
        'args' => array(
            'slug' => array (
                'required' => true
            ),
            'page' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/videos/(?P<page>[0-9]{1,2})', array(
		'methods' => 'GET',
        'callback' => 'get_videos',
        'args' => array(
            'page' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/video/(?P<slug>[a-zA-Z0-9-]+)/(?P<page>[0-9]{1,2})', array(
		'methods' => 'GET',
        'callback' => 'get_videos_by_slug',
        'args' => array(
            'slug' => array (
                'required' => true
            ),
            'page' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/videodetail/(?P<postid>[0-9]*)', array(
		'methods' => 'GET',
        'callback' => 'get_video_detail',
        'args' => array(
            'postid' => array (
                'required' => true
            ),
        )
    ));

    register_rest_route( APIV, '/videodetailmore/(?P<hadpost>[0-9]+(?:-[0-9]+)*)/(?P<page>[0-9]{1,2})', array(
		'methods' => 'GET',
        'callback' => 'get_video_detail_more',
        'args' => array(
            'hadpost' => array (
                'required' => true
            ),
            'page' => array (
                'required' => true
            )
        )
    ));

    register_rest_route( APIV, '/searchs/(?P<slug>[a-zA-Z0-9-]+)/(?P<page>[0-9]{1,2})', array(
		'methods' => 'GET',
        'callback' => 'get_search',
        'args' => array(
            'slug' => array (
                'required' => true
            ),
            'page' => array (
                'required' => true
            )
        )
    ));
    
    register_rest_route( APIV, '/main-menu', array(
		'methods' => 'GET',
		'callback' => 'get_main_menus',
    ));
    
    register_rest_route( APIV, '/hotposts', array(
		'methods' => 'GET',
		'callback' => 'get_hot_posts',
    ));
    
    register_rest_route( APIV, '/feature', array(
		'methods' => 'GET',
		'callback' => 'get_feature_posts',
    ));

    register_rest_route( APIV, '/index/(?P<hadpost>[0-9]+(?:-[0-9]+)*)', array(
		'methods' => 'GET',
        'callback' => 'get_index_posts',
        'args' => array(
            'hadpost' => array (
                'required' => true
            )
        )
    ));
    
    register_rest_route( APIV, '/category_count', array(
		'methods' => 'GET',
		'callback' => 'category_count_callback_api',
	) );
}

// GET SINGLE CONTENT
function get_single_by_id( WP_REST_Request $request ) {
    $obj = [];
	// if ( false === ( $obj = get_transient( 'single-' . $id ))) {
        Posts::$post = (int)$request->get_param('postid');
        $obj = Posts::getSinglePost();
	 	// set_transient( 'single' . $id, $obj, 60*60*24 );
    // }
	return $obj['title'] !== null ? $obj : new WP_Error( 'not-found', 'Không tìm thấy bài viết', array( 'status' => 404 ));
}

// GET SERIES POST
function get_serie_by_id( WP_REST_Request $request ) {
    $obj = [];
    //	if ( false === ( $obj = get_transient( 'serie' . $id ))) {
        $obj = Posts::getSeries((int)$request->get_param('postid'), $request->get_param('trig'));
    //		set_transient( 'serie'.$id, $obj, 60*60*24 ); 
    //	}
	return $obj !== null ? $obj : new WP_Error( 'not-found', 'Không tìm thấy bài viết', array( 'status' => 404 ));
}

// GET POST SUGGEST
function get_post_suggest(WP_REST_Request $request){
    $post = explode('-', $request->get_param('post'));
    $cate = explode('-', $request->get_param('cate'));

    Posts::$args = array(
        'order'	=> 'desc',
        'category__in' => $cate,
        'post__not_in' => $post,
        'posts_per_page' => 6,
        'suppress_filters' => false,
        'orderby' => 'post_views',
        'fields' => ''
    );

    return ['posts'=>Posts::getPostsByFilter(true), 'hadpost'=>Posts::$hadPost];
}

// GET POST RANDOM
function get_post_random(WP_REST_Request $request){
    $post = explode('-', $request->get_param('post'));

    Posts::$args = array(
        'post__not_in' => $post,
        'orderby' => 'rand',
        'posts_per_page' => 1
    );
    return Posts::getPostsByFilter();
}

// GET POST TAGS
function get_post_tags(WP_REST_Request $request){
    Posts::$post    = $request->get_param('postid');
    Posts::$hadPost = explode('-', $request->get_param('hadpost'));
    return ['posts'=>Posts::getPostsTags(), 'hadpost'=>Posts::$hadPost];
}

// GET RELATED POSTS
function get_post_related(WP_REST_Request $request){
    Posts::$term    = $request->get_param('catid');
    Posts::$hadPost = explode('-', $request->get_param('hadpost'));
    return ['posts'=>Posts::getPostsSimilar(), 'hadpost'=>Posts::$hadPost];
}

// GET MORE NEW POST
function get_more_post( WP_REST_Request $request ) {
    $obj            = [];
    $hadPost        = explode('-', $request->get_param('hadpost'));
    $page           = $request->get_param('page');

    Posts::$args    = array('post__not_in' => $hadPost, 'posts_per_page' => 10, 'offset' => $page * 10 , 'post_type' => 'post');
    $obj['posts']   = Posts::getPostsByFilter();

    if(empty($obj['posts'])) return new WP_Error( 'not-found', 'Không tìm thấy', array( 'status' => 404 ));
    $obj['curPage'] = (int)$page;
    $obj['maxPage'] = Posts::$maxPage;

    return $obj;
}

// GET AUTHOR CONTENT
function get_author_by_slug( WP_REST_Request $request ) {
    $obj = [];
    $slug = $request->get_param('slug');
	if ( false === ( $obj = get_transient('author-'.$slug ))) {
        Author::$slug = $slug;
        $obj = Author::getAuthor();
        if(!$obj) return new WP_Error( 'not-found', 'Không tìm thấy tác giả', array( 'status' => 404 ));
		set_transient( 'author-'.$slug, $obj, 60*60*24 ); // cache for 24 hours
	}
	return $obj;
}

// GET MORE POST FORM 1 AUTHOR
function get_author_post( WP_REST_Request $request ) {
    $obj            = [];
    $page           = $request->get_param('page'); 
    $slug           = $request->get_param('slug');
    $hadpost        = $request->get_param('hadpost');

    if($hadpost == 0 && $page == 0){
        Author::$slug = $slug;
        $obj['posts']   = Author::getHotPost();
        $obj['hadpost'] = Posts::$hadPost;
    }else{
        Author::$slug       = $slug;
        Author::$page       = $page;
        Author::$hadpost    = explode('-', $hadpost);

        $obj['posts']   = Author::getNewPost();
        $obj['page']        = (int)$page;
        $obj['maxPage']     = Posts::$maxPage;
    }

    return (empty($obj['posts']))? new WP_Error( 'not-found', 'Không tìm thấy', array( 'status' => 404 )) : $obj;
}

// GET ARCHIVE CONTENT
function get_taxonomy_by_slug( WP_REST_Request $request ) {
    $obj    = [];
    $term   = $request->get_param('term');
    $slug   = $request->get_param('slug');

	if ( false === ( $obj = get_transient('taxonomy-'.$term.'-'.$slug ))) {
        $obj    = get_term_by('slug', $slug, $term);
        $temp   = 'http://placehold.it/720x340/fffdf4/333333?text=+';
        switch($term){
            case 'butdanh':
                $image = get_field('avatar', $obj)['ID'];
                $obj->image = $image ? get_img_from_ID($image) : $temp;
                break;
            case 'category':
                $image = get_field('mobile_bg', $obj);
                $obj->image = $image ? $image : $temp;
                break;
            case 'post_tag':
                Posts::$args = array( 'post_type' => 'post', 'posts_per_page' => 1, 'tax_query' => array( array( 'taxonomy' => $term, 'field' => 'slug', 'terms' => $slug ) ) );
                $post  = Posts::getPostsByFilter(); 
                $obj->description = $obj->name.' - '.$post[0]['excerpt'];
                break;
            default:
                $obj->image = $temp;
                break;
        } 
    	set_transient( 'taxonomy-'.$term.'-'.$slug, $obj, 60*60*24 ); 
	}
    return $obj;
}

function get_archive_by_slug( WP_REST_Request $request ) {
    $obj    = [];
    $slug   = $request->get_param('slug');
    $page   = $request->get_param('page');

    Posts::$args = array(
        'post_type' => 'post',
        'posts_per_page' => 10,
        'offset' => (int)$page * 10,
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => $slug
            )
        )
    );
    $obj['posts']   = Posts::getPostsByFilter();
    $obj['page']    = (int)$page;
    $obj['maxPage'] = Posts::$maxPage;

	return (empty($obj['posts'])) ? new WP_Error( 'not-found', 'Không tìm thấy danh mục', array( 'status' => 404 )) : $obj;
}

// GET TAGS CONTENT
function get_tags_by_slug( WP_REST_Request $request ) {
    $obj = [];
    $slug = $request->get_param('slug');
    $page = $request->get_param('page');
	// if ( false === ( $author_content = get_transient('tags-'.$slug.'-'.$page ))) {
        Posts::$args = array(
            'post_type' => 'post',
            'posts_per_page' => 10,
            'offset' => $page * 10,
            'tax_query' => array(
                array(
                    'taxonomy' => 'post_tag',
                    'field' => 'slug',
                    'terms' => $slug
                )
             )
        );

        $obj['posts'] = Posts::getPostsByFilter();

        if(empty($obj['posts'])) return new WP_Error( 'not-found', 'Không tìm thấy danh mục', array( 'status' => 404 ));

        if($page == 0){
            $tag = get_term_by('slug', $slug, 'post_tag');
            $obj['name'] = $tag->name;
        }

        $obj['page']     = (int)$page;
        $obj['maxPage']     = Posts::$maxPage;

		// set_transient( 'tags-'.$slug.'-'.$page, $obj, 60*60*2 ); 
	// }
	return $obj;
}

// GET CAT VIDEOS CONTENT
function get_videos( WP_REST_Request $request ) {
    $page = $request->get_param('page');

    Video::$offset  = $page * 10;
    $obj['posts']   = Video::getVideos();
    $obj['page'] = (int)$page;
    $obj['maxPage'] = Posts::$maxPage;

	return (empty($obj['posts']))? new WP_Error( 'not-found', 'Không tìm thấy danh mục video', array( 'status' => 404 )):$obj;
}

// GET CAT VIDEO CONTENT
function get_videos_by_slug( WP_REST_Request $request ) {
    $slug   = $request->get_param('slug');
    $page   = $request->get_param('page');

    Posts::$args = array(
        'post_type'         => 'video',
        'posts_per_page'    => 10,
        'offset'            => $page * 10,
        'tax_query'         => array(
            array(
                'taxonomy'  => 'video_category',
                'field'     => 'slug',
                'terms'     => $slug
            )
        )
    );

    $obj['posts']       = Posts::getPostsByFilter();
    $obj['page']     = (int)$page;
    $obj['maxPage']     = Posts::$maxPage;

	return (empty($obj['posts']))? new WP_Error( 'not-found', 'Không tìm thấy danh mục video', array( 'status' => 404 )):$obj;
}

// GET VIDEO DETAIL CONTENT
function get_video_detail( WP_REST_Request $request ) {
    $obj    = [];
    $id     = $request->get_param('postid');
	// if ( false === ( $author_content = get_transient('videos-'.$slug ))) {
        Video::$id = $id;
        $obj = Video::getSingleVideo();
		// set_transient( 'videos-'.$slug, $obj, 60*60*2 ); 
	// }
	return (empty($obj))? new WP_Error( 'not-found', 'Không tìm thấy video này', array( 'status' => 404 )):$obj;
}

// GET VIDEO DETAIL MORE
function get_video_detail_more( WP_REST_Request $request ) {
    $page    = $request->get_param('page');
    $hadPost = explode('-', $request->get_param('hadpost'));

    Posts::$args = array(
        'post_type'         => 'video',
        'post__not_in'      => $hadPost,
        'posts_per_page'    => 10,
        'offset'            => $page * 10
    );

    $obj['posts']       = Posts::getPostsByFilter(); 
    $obj['page']        = (int)$page;
    $obj['maxPage']     = Posts::$maxPage;

	return (empty($obj['posts']))? new WP_Error( 'not-found', 'Không tìm thấy danh mục video', array( 'status' => 404 )):$obj;
}

// GET SEARCH
function get_search( WP_REST_Request $request ){
    $obj     = [];
    $slug    = $request->get_param('slug');
    $page    = $request->get_param('page');
    Posts::$args = array(
        'post_type' => 'post',
        's' => str_replace('-',' ', $slug),
        'posts_per_page' => 10,
        'offset'      => $page * 10,
        'post_status' => 'publish',
        'orderby'     => 'title', 
        'order'       => 'ASC'        
    );

    $obj['posts'] = Posts::getPostsByFilter();
    $obj['count'] = (int)Posts::$countPost;
    $obj['page'] = (int)$page;
    $obj['maxPage'] = Posts::$maxPage;

    return $obj;
}

// GET MENU
function get_main_menus() {
    $result = [];
        foreach(A_GROUP_CATEGORY_IDS as $group_id):
            $cats           = get_category((int)$group_id); 
            $objs           = array();         
            $objs['slug']   = '/'.$cats->slug;
            $objs['title']  = $cats->cat_name;

            $sub_categories = get_categories(array('parent' => $group_id));
            if(!empty($sub_categories)):
                foreach($sub_categories as $category):
                    $cat            = get_category((int)$category); 
                    $objs[]         = $cat;
                endforeach;
            endif;
            $result[] = $objs;
        endforeach;
	return $result;
}

// GET HOT POSTS
function get_hot_posts(){
    Posts::$args = array(
        'posts_per_page'   => 12,
        'order'	=> 'desc',
        'post_type' => 'post',
        'date_query' => array(
            array(
                'after' => '3 days ago'
            )
        ),
        'suppress_filters' => false,
        'orderby' => 'post_views',
    );
    return Posts::getPostsByFilter();
}

// GET INDEX POSTS
function get_feature_posts(){
    $obj['posts']   = Index::featured();
    $obj['hadpost'] = Posts::$hadPost;
    return  $obj;
}

function get_index_posts(WP_REST_Request $request){
    Posts::$hadPost = explode('-', $request->get_param('hadpost')); 
    $cate =  array(
        9535,   //Giải trí
        9538,   //Khám phá
        9544,   //Sống đẹp
        9541    //Sống deep
    );

    $obj['newest_1'] = Index::newest();
    $obj['trending'] = Index::trending();
    $obj['newest_2'] = Index::newest();
    Index::$cate = $cate[0]; $obj['giai_tri'] = Index::categoryList();
    Index::$cate = $cate[1]; $obj['kham_pha'] = Index::categoryList();
    Index::$cate = $cate[2]; $obj['song_dep'] = Index::categoryList();
    Index::$cate = $cate[3]; $obj['song_deep'] = Index::categoryList();

    $obj['hadPost'] = Posts::$hadPost;
    return $obj;
}

function category_count_callback_api(){
	$args = array(
	    'posts_per_page' => false,
	    'post_type' => 'post',
	    'post_status' => 'publish',
	    'orderby' => 'date',
	    'order' => 'DESC',
	    'date_query' => array(
	        array(
	            'after' => '100 days ago'
	        )
	    )
	);

	$latest_posts = new WP_Query($args);
	$result = [];
	if($latest_posts->have_posts()){
		while($latest_posts->have_posts()){
			$latest_posts->the_post();
			$categories = get_the_category(get_the_ID());
			foreach($categories as $category){
				$key = $category->term_id;
				$result[$key] =  $result[$key] ? $result[$key]+1 : 1;
			}
		}
	}
	$response = new WP_REST_Response( $result );
	return $response;
}