<?php
class Terms extends TParser{
	public static $taxonomy; 		// Taxonomy slug
	public static $post; 			// Post ID
	public static $term; 			// Term ID
    public static $format = '<a routerLink=\'{{link}}\' title=\'{{name}}\'>{{name}}</a>';
    
    public static function get_id_by_slug($page_title, $post_type ='post' , $output = OBJECT) {
        global $wpdb;
            $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $page_title, $post_type));
            if ( $post )
                return $post;
        return null;
    }
	
	public static function get_primary_cat( $parser = true ){
		// init
		static::$post 		= (static::$post)? static::$post : get_the_ID();
		static::$taxonomy 	= get_post_taxonomies(static::$post)[0];
		
		// check primary key yoast
		$primary = get_post_meta(static::$post, '_yoast_wpseo_primary_category', true);
		if ( $primary!= null ) {
			static::$term = $primary;
			return ($parser)? static::parser_term($primary) : $primary;
		}
		
		// get list term ID from post ID
		$args	= array('fields' => 'ids');
		$terms 	= wp_get_post_terms(static::$post, static::$taxonomy, $args);
		
		if(count($terms) == 1){
			static::$term = $terms[0];
			return ($parser)? static::parser_term($terms[0]) : $terms[0];
		}
		
		foreach ($terms as $term){
			static::$term = $term;
			if(self::cat_have_child()){
				static::$term = $term;
				return ($parser)? static::parser_term($term): $term;
			}
		}
	}
	
	public static function get_parent_cat( $parser = true ){
		$term = get_term(static::$term);
		if( !is_wp_error($term) ){
			static::$term = $term->parent;
			return ($parser)? static::parser_term() : $term->parent;
		}
	}
	
	public static function get_all_cate( $parser = true ){
		// init
		static::$post 		= (static::$post)? static::$post : get_the_ID();
		static::$taxonomy 	= get_post_taxonomies(static::$post)[0];
		
		// get list term ID from post ID
		$args	= array('fields' => 'ids');
		$terms 	= wp_get_post_terms(static::$post, static::$taxonomy, $args);
		
		$result	= '';
		foreach ($terms as $term){
			static::$term 	= $term;
			$result		   .= static::parser_term();
		}
		return $result;
	}
	
	public static function cat_have_child(){
		if(empty(get_term_children(static::$term, static::$taxonomy)))
			return true;
		else
			return false;
    }

	public static function parser_term(){
		$term_id 	= (int)static::$term;
		$term 		= get_term($term_id); 
		
		if( !is_wp_error($term) ){
            $obj['id']	    = $term->term_id;
			$obj['name']	= $term->name;
            $obj['count']	= $term->count;
			$obj['link']	= '/'.TParser::stream_domain(get_term_link($term_id));
			return $obj;
		}
    }
    
    function get_post_format_icon($post = null){
        $format = get_post_format(self::$post) ? : 'standard';
        $html = '';
        switch($format){
            case 'gallery':
            case 'image':
                $html = '<i class="icon-photo"></i>';
                break;
            case 'video':
                $html = '<i class="icon-camera"></i>';
                break;
            default:
                $html = '';
                break;
        }
        return $html;
    }
}