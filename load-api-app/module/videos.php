<?php
class Video extends TParser{
    public static $slug;
    public static $id;
    public static $offset;

    public static function getSingleVideo(){
        // $id = Terms::get_id_by_slug( static::$slug, 'video' );
        $video = get_post(Video::$id);
        Terms::$post     = Video::$id;

        $obj['id']      = Video::$id;
        $obj['title']   = $video->post_title;
        $obj['date']    = $video->post_date;
        $obj['excerpt'] = $video->post_excerpt;
        $obj['cate']    = Terms::get_primary_cat();
        $obj['content'] = $video->post_content;

        return $obj;
    }

    public static function getVideos(){
        Posts::$args = array( 
            'posts_per_page' => 10,
            'offset' => self::$offset,
            'post_type' => 'video',
            'suppress_filters' => false
        );
        return Posts::getPostsByFilter();
    }

    public static function getVideosBySlug(){
        Posts::$args = array( 
            'posts_per_page' => 10,
            'post_type' => 'video',
            'suppress_filters' => false,
            'tax_query' => array(
                array(
                    'taxonomy' => 'video_category',
                    'field' => 'slug',
                    'terms' => static::$slug
                ),
            )
        );
        return Posts::getPostsByFilter();
    }
}