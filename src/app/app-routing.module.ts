import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AppCustomPreloader } from './app-routing-loader';

import { IndexComponent } from './index/index.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { IndexResolver } from './caller/index.resolver';
import { ErrorComponent } from './error/error.component';

const routesConfig: Routes = [
    {
        path: '',
        component: IndexComponent,
        resolve: {
            index: IndexResolver
        }
    },
    {
        path: 'error',
        component: ErrorComponent
    },
    {
        path: 'not-found',
        component: NotfoundComponent
    },
    {
        path: 'users',
        loadChildren: "../app/users/users.module#UsersModule",
    },
    {
        path: 'tim-kiem',
        loadChildren: "../app/search/search.module#SearchModule",
        data: { preload: true }
    },
    {
        path: 'but-danh',
        loadChildren: "../app/author/author.module#AuthorModule",
        data: { preload: true }
    },
    {
        path: 'tag',
        loadChildren: "../app/tags/tags.module#TagsModule",
        data: { preload: true }
    },
    {
        path: 'video',
        loadChildren: "../app/video/video.module#VideoModule",
        data: { preload: true }
    },
    {
        path: 'video-category',
        loadChildren: "../app/video-category/video-category.module#VideoCategoryModule",
        data: { preload: true }
    },
    {
        path: '',
        loadChildren: "../app/single/single.module#SingleModule",
        data: { preload: true }
    },
    {
        path: '**',
        component: NotfoundComponent
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routesConfig, {
            preloadingStrategy: AppCustomPreloader,
            // scrollPositionRestoration: 'enabled',
            // anchorScrolling: 'enabled',
            initialNavigation: 'enabled'
        }),
        CommonModule
    ],
    exports: [RouterModule],
    providers: [AppCustomPreloader, IndexResolver]
})

export class AppRoutingModule { }
