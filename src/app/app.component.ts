import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { SwPush, SwUpdate } from "@angular/service-worker";
import { OAuthService } from 'angular-oauth2-oidc';
import { isPlatformBrowser } from '@angular/common';
import { JwksValidationHandler } from 'angular-oauth2-oidc';
import { VariableService } from './caller/variable.service';
import { authConfig } from './globals';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {

    private isBrowser;
    message: boolean;
    loading: boolean;

    constructor(
        private swUpdate: SwUpdate,
        private oauthService: OAuthService,
        private loader: VariableService,
        @Inject(PLATFORM_ID) private platformId: object
    ) {
        this.isBrowser = isPlatformBrowser(this.platformId);
    }

    ngOnInit(): void {
        if (this.swUpdate.isEnabled) {
            this.swUpdate.available.subscribe(() => {
                window.location.reload();
            });
        }

        // Connect User Core
        if (this.isBrowser) this.configureWithNewConfigApi();

        // Loading
        this.loader.loadState.subscribe(res => this.loading = res);

        // Detect mobies
        let isMobile = /Android|iPhone|iPad/i.test(window.navigator.userAgent);
    }

    onActivate() {}

    private configureWithNewConfigApi() {
        this.oauthService.configure(authConfig);
        this.oauthService.setStorage(localStorage);
        this.oauthService.tokenValidationHandler = new JwksValidationHandler();
        this.oauthService.loadDiscoveryDocument();

        this.oauthService.setupAutomaticSilentRefresh();
    }
}