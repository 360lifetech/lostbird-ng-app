import { NgModule, PLATFORM_ID, APP_ID, Inject } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule, BrowserTransferStateModule } from "@angular/platform-browser";
import { isPlatformBrowser } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { IndexModule } from "./index/index.module";
import { HeaderModule } from "./header/header.module";

import { HttpService } from "./caller/http.service";
import { VariableService } from "./caller/variable.service";
import { CacheService } from "./caller/cache.service";
import { LocalService } from "./caller/local.service";
import { EmbedService } from './caller/embed.service';

import { AppComponent } from "./app.component";
import { FooterComponent } from "./footer/footer.component";
import { NotfoundComponent } from "./notfound/notfound.component";
import { ErrorComponent } from "./error/error.component";

import { OAuthModule } from "angular-oauth2-oidc";
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
    declarations: [
        AppComponent,
        FooterComponent,
        NotfoundComponent,
        ErrorComponent
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: "appLostbird" }),
        BrowserTransferStateModule,
        BrowserAnimationsModule,
        OAuthModule.forRoot(),
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        HeaderModule,
        IndexModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    ],
    providers: [
        HttpService,
        VariableService,
        CacheService,
        LocalService,
        EmbedService
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule {
    constructor(
        @Inject(PLATFORM_ID) private platformId: object,
        @Inject(APP_ID) private appId: string
    ) {
        const platform = isPlatformBrowser(this.platformId) ? "in the browser" : "on the server";
    }
}
