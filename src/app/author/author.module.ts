import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { NgxJsonLdModule } from "@ngx-lite/json-ld";
import { AppShellModule } from "../directives/appShell.module";

import { LoaderModule } from "../caller/loader/loader.module";
import { TaxResolver } from "../caller/tax.resolver";

import { AuthorComponent } from "./author.component";

const routes: Routes = [
    {
        path: ":slug",
        component: AuthorComponent,
        resolve: {
            tax: TaxResolver
        }
    }
];

@NgModule({
    declarations: [AuthorComponent],
    imports: [CommonModule, RouterModule.forChild(routes), LoaderModule, NgxJsonLdModule, AppShellModule],
    providers: [TaxResolver]
})

export class AuthorModule { }
