import { Injectable } from '@angular/core';

declare namespace instgrm {
    namespace Embeds {
        function process(): void;
    }
}

declare const FB: any;

@Injectable()

export class EmbedService {

    public embedInstagram(): void {
        setTimeout(function () {
            if (window['instgrm']) instgrm.Embeds.process()
        }, 100);
    }

    public embedFacebook(): void {
        setTimeout(function () {
            FB.init({
                appId: '181434419087855',
                cookie: false,
                xfbml: true,
                version: 'v3.2'
            });
        }, 100)
    }

    public embedTwitter(): void {
        setTimeout(function () {
            (<any>window).twttr = (function (d, s, id) {
                let js: any, fjs = d.getElementsByTagName(s)[0], t = (<any>window).twttr || {};
                if (d.getElementById(id)) return t;
                js = d.createElement(s);
                js.id = id;
                js.src = "https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);

                t._e = [];
                t.ready = function (f: any) {
                    t._e.push(f);
                };
                return t;
            }(document, "script", "twitter-wjs"));
            if ((<any>window).twttr.ready()) (<any>window).twttr.widgets.load();
        }, 100);
    }
}