import { Injectable, Optional, Inject } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { APP_BASE_HREF } from '@angular/common'

import { throwError, Observable } from 'rxjs'
import { catchError, tap, finalize } from 'rxjs/operators'

import { VariableService } from './variable.service'
import { server, api, Post, Posts, Related, NewPost, Cate, Comment } from '../globals'

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()

export class HttpService {
    public cache$: Observable<any>;
    public API_ENDPOINT = '';

    constructor(
        private http: HttpClient,
        private loader: VariableService,
        @Optional() @Inject(APP_BASE_HREF) origin: string) {
        this.API_ENDPOINT = `${origin}${this.API_ENDPOINT}`
    }

    findPostFearture(): Observable<NewPost> {
        return this.http.get<NewPost>(`${server}${api}/feature`);
    }

    findPostBySlug(slug: string, term: string): Observable<Post> {
        let id = slug.match(/[0-9]*.html/i)[0].replace('.html', '');
        return this.http.get<Post>(`${server}${api}/${term}/${id}`);
    }

    findTaxBySlug(slug: string, term: string): Observable<Cate> {
        return this.http.get<Cate>(`${server}${api}/taxonomy/${term}/${slug}`);
    }

    getData(): Observable<any> {
        return this.http.get<any>(server + api + this.API_ENDPOINT).pipe(
            tap(res => { })
        );
    }

    getDataComment(): Observable<any> {
        return this.http.get<any>(server + 'wp/v2' + this.API_ENDPOINT).pipe(
            tap(res => { })
        );
    }

    postDataComment(data: Comment): Observable<any> {
        return this.http.post<any>(server + 'wp/v2/comments', data, httpOptions);
    }

    getPost(): Observable<Post> {
        this.showLoader(true);
        return this.http.get<Post>(server + api + this.API_ENDPOINT).pipe(
            tap(post => { }),
            catchError(err => { return throwError(err) }),
            finalize(() => { this.showLoader(false) })
        );
    }

    getPosts(): Observable<Posts[]> {
        this.showLoader(true);
        return this.http.get<Posts[]>(server + api + this.API_ENDPOINT).pipe(
            tap(post => { }),
            catchError(err => { return throwError(err) }),
            finalize(() => { this.showLoader(false) })
        );
    }

    getRelated(): Observable<Related> {
        this.showLoader(true);
        return this.http.get<Related>(server + api + this.API_ENDPOINT).pipe(
            tap(post => { }),
            catchError(err => { return throwError(err) }),
            finalize(() => { this.showLoader(false) })
        );
    }

    getNewPost(): Observable<NewPost> {
        this.showLoader(true);
        this.showMore(false);
        return this.http.get<NewPost>(server + api + this.API_ENDPOINT).pipe(
            tap(post => { this.showMore(post.page + 1 >= post.maxPage) }),
            catchError(err => { return throwError(err) }),
            finalize(() => { this.showLoader(false) })
        );
    }

    public showLoader(val: boolean): void {
        this.loader.show(val);
    }

    public showMore(val: boolean): void {
        this.loader.more(val);
    }
}
