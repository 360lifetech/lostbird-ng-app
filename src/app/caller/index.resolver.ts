import { Injectable } from "@angular/core"
import { Resolve } from "@angular/router"
import { Observable } from "rxjs"
import { HttpService } from "./http.service"
import { CacheService } from "./cache.service";
import { VariableService } from "./variable.service";

import { NewPost } from "../globals"

@Injectable()
export class IndexResolver implements Resolve<NewPost> {

    constructor(
        private httpService: HttpService,
        private cacheService: CacheService,
        private loader: VariableService
    ) { }

    resolve(): Observable<NewPost> {
        this.loader.show(true);
        return this.cacheService.get('homepage', this.httpService.findPostFearture());
    }
}