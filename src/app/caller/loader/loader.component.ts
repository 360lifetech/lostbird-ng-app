import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs';

import { VariableService } from '../variable.service';
import { LoaderState, MoreState } from '../../globals';

@Component({
    selector: 'angular-loader',
    templateUrl: 'loader.component.html'
})

export class LoaderComponent implements OnInit {

    load: boolean = true;
    more: boolean = false;

    private subLoad: Subscription;
    private subMore: Subscription;

    @Output() loadClick = new EventEmitter();

    constructor(
        private loaderService: VariableService
    ) { }

    ngOnInit() { 
        this.subMore = this.loaderService.moreState.subscribe(
            (more: MoreState) => {
                this.more = more.show;  
            }
        );
        this.subLoad = this.loaderService.loaderState.subscribe(
            (load: LoaderState) => {
                this.load = load.show;
            }
        );
    }

    ngOnDestroy() {
        this.subMore.unsubscribe();
        this.subLoad.unsubscribe();
    }

    loadData() {
        this.loadClick.emit();
    }
}
