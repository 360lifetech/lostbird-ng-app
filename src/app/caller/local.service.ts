import { Injectable } from '@angular/core';

@Injectable()

export class LocalService {

    findAll(key: string): string[] {
        if (localStorage.getItem(key) != null) {
            return JSON.parse(localStorage.getItem(key));
        }
        return [];
    }

    add(title: any, key: string): void {
        if (localStorage.getItem(key) == null) {
            let list: object[] = [];
            switch (key) {
                case 'lostbird-post':
                    if (title !== null) list.push(title);
                    break;
                case 'lostbird-history':
                    list.push({ link: title });
                    break;
                case 'lostbird-postrand':
                    list.push({ rand: title });
                    break;
                default:
                    list.push({ id: title, count: 1 });
                    break;
            }
            localStorage.setItem(key, JSON.stringify(list));
        } else {
            let list: object[] = JSON.parse(localStorage.getItem(key));
            switch (key) {
                case 'lostbird-post':
                    if (list.indexOf(title) === -1 && title !== null) {
                        if (list.length > 50)
                            list.shift();
                        list.push(title);
                    }
                    break;
                case 'lostbird-history':
                    list[0]['link'] = title;
                    break;
                case 'lostbird-postrand':
                    list[0]['rand'] = title;
                    break;
                default:
                    let found = list.findIndex(t => t['id'] == title);
                    if (found === -1)
                        list.push({ id: title, count: 1 });
                    else
                        list[found]['count'] += 1;
                    break;
            }
            localStorage.setItem(key, JSON.stringify(list));
        }
    }

    findMax(key: string): number[] {
        if (localStorage.getItem(key) == null) return [];

        let lists = JSON.parse(localStorage.getItem(key));
        let max: number[] = [];
        for (var i = 0; i < 3; i++) {
            let highest = lists.reduce((max, current) => current.count > max.count ? current : max, { count: -1 });
            if (highest.id === undefined)
                return max;
            max.push(highest.id);
            lists = lists.filter(function (obj) { return obj.id !== highest.id });
        }
        return max;
    }

    delete(index: number, key: string): void {
        let list: object[] = JSON.parse(localStorage.getItem(key));
        list.splice(index, 1);
    }

    countArray() {
        var arr = [2, 259, 2, 262, 15472, 260, 2, 36, 36, 36, 36, 36];
        var a = [], b = [], prev;
        arr.sort();
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] !== prev) {
                a.push(arr[i]);
                b.push(1);
            } else {
                b[b.length - 1]++;
            }
            prev = arr[i];
        }
        return [a, b];
    }
}