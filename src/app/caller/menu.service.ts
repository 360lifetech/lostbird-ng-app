import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })

export class MenuService {

  menus: Object = [
    {
      slug: '/video', title: 'Video', id: 9531, child: [
        { slug: '/video-category/de-thuong/', title: 'Dễ thương' },
        { slug: '/video-category/doi-song-xa-hoi/', title: 'Đời sống & Xã hội' },
        { slug: '/video-category/giai-tri/', title: 'Giải trí' },
        { slug: '/video-category/nguoi-noi-tieng/', title: 'Người nổi tiếng' },
        { slug: '/video-category/quang-cao/', title: 'Quảng cáo' },
      ]
    },
    {
      slug: '/giai-tri-cung-lac', title: 'Giải trí', id: 9535, child: [
        { slug: '/giai-tri-cung-lac/nsfw/', title: '16+', id: 2 },
        { slug: '/giai-tri-cung-lac/am-nhac', title: 'Âm nhạc', id: 262 },
        { slug: '/giai-tri-cung-lac/de-thuong', title: 'Dễ thương', id: 29 },
        { slug: '/giai-tri-cung-lac/game/', title: 'Game', id: 15472 },
        { slug: '/giai-tri-cung-lac/giai-tri/', title: 'Hài hước', id: 38 },
        { slug: '/giai-tri-cung-lac/kinh-di/', title: 'Kinh dị', id: 36 },
        { slug: '/giai-tri-cung-lac/phim-anh/', title: 'Phim ảnh', id: 259 },
        { slug: '/giai-tri-cung-lac/showbiz/', title: 'Showbiz', id: 260 },
        { slug: '/giai-tri-cung-lac/the-thao/', title: 'Thể thao', id: 27 },
      ]
    },
    {
      slug: '/kham-pha-cung-lac', title: 'Khám phá', id: 9538, child: [
        { slug: '/kham-pha-cung-lac/am-thuc/', title: 'Ẩm thực', id: 28 },
        { slug: '/kham-pha-cung-lac/cuoc-song', title: 'Cuộc sống', id: 947 },
        { slug: '/kham-pha-cung-lac/doc-la', title: 'Độc lạ', id: 22 },
        { slug: '/kham-pha-cung-lac/du-lich/', title: 'Du lịch', id: 39 },
        { slug: '/kham-pha-cung-lac/lich-su/', title: 'Lịch sử', id: 5273 },
        { slug: '/kham-pha-cung-lac/nhiep-anh/', title: 'Nhiếp ảnh', id: 34 },
        { slug: '/kham-pha-cung-lac/thien-nhien/', title: 'Thiên nhiên', id: 20 },
        { slug: '/kham-pha-cung-lac/tin-tuc/', title: 'Tin tức', id: 31 },
        { slug: '/kham-pha-cung-lac/videos/', title: 'Video', id: 40 },
      ]
    },
    {
      slug: '/song-deep-cung-lac', title: 'Sống \"deep\"', id: 9544, child: [
        { slug: '/song-deep-cung-lac/beauty/', title: 'Beauty', id: 37 },
        { slug: '/song-deep-cung-lac/nghe-thuat/', title: 'Nghệ thuật', id: 21 },
        { slug: '/song-deep-cung-lac/sach/', title: 'Sách', id: 15970 },
        { slug: '/song-deep-cung-lac/tinh-yeu/', title: 'Tình yêu', id: 297 },
        { slug: '/song-deep-cung-lac/voices/', title: 'Voices', id: 148 },
      ]
    },
    {
      slug: '/song-dep-cung-lac', title: 'Sống \"đẹp\"', id: 9541, child: [
        { slug: '/song-dep-cung-lac/anh-em/', title: 'Anh em', id: 30 },
        { slug: '/song-dep-cung-lac/cau-vong/', title: 'Cầu vồng', id: 946 },
        { slug: '/song-dep-cung-lac/chi-em/', title: 'Chị em', id: 35 },
        { slug: '/song-dep-cung-lac/handmade/', title: 'Handmade', id: 25 },
        { slug: '/song-dep-cung-lac/thoi-trang/', title: 'Thời trang', id: 261 },
      ]
    }
  ];

  constructor() { }

  getMenu() {
    return this.menus;
  }

  getMenuChild(num: number) {
    return this.menus[num];
  }
}
