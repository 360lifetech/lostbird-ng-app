import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve} from '@angular/router';
import { Post } from '../globals';
import { Observable} from 'rxjs';
import { HttpService } from './http.service';
import { VariableService } from './variable.service';
import { CacheService } from './cache.service';



@Injectable()
export class PostResolver implements Resolve<Post> {

    constructor(
        private httpService: HttpService,
        private cacheService: CacheService,
        private loader: VariableService
    ) { }

    resolve(route: ActivatedRouteSnapshot): Observable<Post> {
        this.loader.show(true);
        let n = route.url.length;
        let slug = route.url[n - 1].path;

        const COURSE_KEY = 'single-' + slug;
        return this.cacheService.get(COURSE_KEY, this.httpService.findPostBySlug(slug, 'single'))
    }
}