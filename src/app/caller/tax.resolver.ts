import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { throwError, EMPTY, Observable, of } from 'rxjs';
import { tap, catchError, finalize } from 'rxjs/operators';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { VariableService } from './variable.service';
import { Cate } from '../globals';


@Injectable()
export class TaxResolver implements Resolve<Cate> {

    constructor(
        private router: Router,
        private httpService: HttpService,
        private loader: VariableService,
        private transferState: TransferState
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Cate> {

        this.loader.changeLoadState(false);

        const slug = route.params['slug']; 
        const COURSE_KEY = makeStateKey<Cate>('taxonomy-' + slug);

        let tax = state.url.split('/')[1];
        let term = 'category'

        switch (tax) {
            case 'but-danh':
                term = 'butdanh';
                break;
            case 'tag':
                term = 'post_tag';
                break;
            case 'video-category':
                term = 'video_category';
                break;
            default:
                term = 'category';
                break;
        }
        this.loader.show(true);

        if (this.transferState.hasKey(COURSE_KEY)) {
            const res = this.transferState.get<Cate>(COURSE_KEY, null);
            this.transferState.remove(COURSE_KEY);
            return of(res);
        }
        else {
            return this.httpService.findTaxBySlug(slug, term).pipe(
                tap(res => { this.transferState.set(COURSE_KEY, res); }),
                catchError(err => {
                    if (err.status == 404) {
                        this.router.navigate(['not-found']); return EMPTY;
                    }
                    else if (err.status == 0) {
                        this.router.navigate(['error']); return EMPTY;
                    }
                    else {
                        return throwError(err);
                    }
                }),
                finalize(() => {
                    setTimeout(() => {
                        this.loader.changeLoadState(true);
                    }, 400);
                })
            )
        }
    }
}