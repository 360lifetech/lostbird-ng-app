import { Injectable, EventEmitter } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

import { LoaderState, MoreState } from '../globals';

@Injectable()

export class VariableService {

    // Local variable Loader
    private loaderSubject = new Subject<LoaderState>();
    loaderState = this.loaderSubject.asObservable();
    show(val: boolean) {
        this.loaderSubject.next(<LoaderState>{ show: val });
    }

    // Local variable loadmore
    private moreSubject = new Subject<MoreState>();
    moreState = this.moreSubject.asObservable();
    more(val: boolean) {
        this.moreSubject.next(<MoreState>{ show: val });
    }

    private loading = new BehaviorSubject<boolean>(false);
    loadState = this.loading.asObservable();
    changeLoadState(val: boolean) {
        this.loading.next(val);
    }

    private random = new BehaviorSubject<boolean>(false);
    randomState = this.random.asObservable();
    changeRandomState(val: boolean) {
        this.random.next(val);
    }

    private scroll = new Subject<boolean>();
    scrollState = this.scroll.asObservable();
    changeScrollState(val: boolean) {
        this.scroll.next(val);
    }

    // Local variable using event
    // onGetData: EventEmitter<boolean> = new EventEmitter(false);
    // changeData(val: boolean) {
    //     this.onGetData.emit(val);
    // }
}
