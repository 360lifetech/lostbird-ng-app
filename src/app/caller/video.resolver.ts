import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { Post } from '../globals';
import { throwError, EMPTY, Observable, of } from 'rxjs';
import { HttpService } from './http.service';
import { tap, catchError, finalize } from 'rxjs/operators';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { VariableService } from './variable.service';


@Injectable()
export class VideoResolver implements Resolve<Post> {

    constructor(
        private router: Router,
        private httpService: HttpService,
        private loader: VariableService,
        private transferState: TransferState
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Post> {
        this.loader.changeLoadState(false);

        const slug = route.params['slug'];
        const COURSE_KEY = makeStateKey<Post>('videodetail-' + slug);

        if (this.transferState.hasKey(COURSE_KEY)) {
            const res = this.transferState.get<Post>(COURSE_KEY, null);
            this.transferState.remove(COURSE_KEY);
            return of(res);
        }
        else {
            return this.httpService.findPostBySlug(slug, 'videodetail').pipe(
                tap(res => {
                    this.transferState.set(COURSE_KEY, res);
                }),
                catchError(err => {
                    if (err.status == 404) {
                        this.router.navigate(['not-found']); return EMPTY;
                    }
                    else if (err.status == 0) {
                        this.router.navigate(['error']); return EMPTY;
                    }
                    else {
                        return throwError(err);
                    }
                }),
                finalize(() => {
                    setTimeout(() => {
                        this.loader.changeLoadState(true);
                    }, 400);
                })
            )
        }
    }
}