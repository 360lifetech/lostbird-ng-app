import { NgModule } from "@angular/core";

import { AppShellNoRenderDirective } from './appShellNoRender.directive';
import { AppShellRenderDirective } from './appShellRender.directive';

@NgModule({
    declarations: [AppShellNoRenderDirective, AppShellRenderDirective],
    exports:[AppShellNoRenderDirective, AppShellRenderDirective]
})

export class AppShellModule { }
