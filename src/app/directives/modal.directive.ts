import { Directive, ElementRef, Renderer2, AfterViewInit } from "@angular/core";

@Directive({
    selector: '[modal]'
})
export class ModalDirective implements AfterViewInit {

    constructor(
        private el: ElementRef,
        private render: Renderer2
    ) { }

    ngAfterViewInit() {
        this.render.listen(this.el.nativeElement, 'click', () => {
            let target = this.el.nativeElement.getAttribute('data-target');
            let elm = document.querySelector('#' + target);

            // Open popup
            this.render.addClass(elm, 'open');
            // Set overlay
            let over = this.render.createElement('div');
            this.render.addClass(over, 'modal-overlay');
            this.render.setStyle(over, 'z-index', 1002);
            this.render.setStyle(over, 'display', 'block');
            this.render.setStyle(over, 'opacity', 0.5);
            this.render.setStyle(elm.parentElement, 'z-index', 10);
            this.render.setStyle(document.body, 'overflow', 'hidden');
            this.render.appendChild(elm.parentElement, over);

            this.render.listen(elm.parentElement.querySelector('.modal-overlay'), 'click', () => {
                this.render.removeChild(elm.parentElement, over);
                this.render.removeClass(elm, 'open');
                this.render.removeStyle(elm.parentElement, 'z-index');
                this.render.removeStyle(document.body, 'overflow');
            })

            this.render.listen(elm.parentElement.querySelector('.modal-close'), 'click', () => {
                this.render.removeChild(elm.parentElement, over);
                this.render.removeClass(elm, 'open');
                this.render.removeStyle(elm.parentElement, 'z-index');
                this.render.removeStyle(document.body, 'overflow');
            })
        });
    }
}