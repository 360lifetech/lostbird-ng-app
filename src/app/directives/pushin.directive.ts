import { Directive, ElementRef, HostListener, Renderer2 } from "@angular/core";

@Directive({
    selector: '[pushin]'
})
export class PushInDirective {

    constructor(
        private el: ElementRef,
        private render: Renderer2
    ) { }

    @HostListener('window:scroll', ['$event']) onMouseScroll() {
        let pinned = this.el.nativeElement.querySelector('.pushpin-b');
        if (pinned !== null) {
            if (window.scrollY > this.el.nativeElement.offsetTop && window.scrollY < this.el.nativeElement.offsetTop + this.el.nativeElement.offsetHeight)
                this.render.addClass(pinned, 'pinned');
            else
                this.render.removeClass(pinned, 'pinned');
        }
    }
}