import { NgModule } from "@angular/core";

import { PushInDirective } from './pushin.directive';

@NgModule({
    declarations: [PushInDirective],
    exports: [PushInDirective]
})

export class PushInModule { }
