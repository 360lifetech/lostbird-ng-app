import { Directive, ElementRef, Renderer2, AfterViewInit } from "@angular/core";
import { HttpService } from '../caller/http.service';

@Directive({
    selector: '[reply-comment]'
})
export class ReplyDirective implements AfterViewInit {

    constructor(
        private http: HttpService,
        private el: ElementRef,
        private render: Renderer2
    ) { }

    ngAfterViewInit() {
        this.render.listen(this.el.nativeElement, 'click', () => {
            let target = this.el.nativeElement.getAttribute('data-target');
            let parentNode = document.querySelector('#' + target);
            let parentChild = parentNode.querySelector('.content-comment');
            let refChild = parentNode.querySelector('.action-comment');

            let formNode = this.render.createElement('div');        this.render.addClass(formNode, 'comment-form'); this.render.addClass(formNode, 'comment-child'); this.render.addClass(formNode, 'show-comment');

            let imgNode = this.render.createElement('img');         this.render.setAttribute(imgNode, 'src', '../../assets/images/thumbnails/avatar/avatar-1.jpg');
            let linkNode = this.render.createElement('a');          this.render.addClass(linkNode, 'avatar-form'); this.render.appendChild(linkNode, imgNode); 

            let text = this.render.createText('Gửi bình luận');     
            let button = this.render.createElement('button');       this.render.addClass(button, 'btn-submit-comment'); this.render.setAttribute(button, 'type', 'submit'); this.render.appendChild(button, text);
            let textarea = this.render.createElement('textarea');   this.render.addClass(textarea, 'input-comment');
            let inputNode = this.render.createElement('div');       this.render.addClass(inputNode, 'input-form'); this.render.appendChild(inputNode, textarea); this.render.appendChild(inputNode, button);
            
            
            this.render.appendChild(formNode, linkNode);
            this.render.appendChild(formNode, inputNode);
            this.render.insertBefore(parentChild, formNode, refChild);
            this.render.setStyle(refChild, 'display', 'none');

            this.render.listen(textarea, 'focus', () => {
                this.render.addClass(formNode, 'active-btn');
            });

            this.render.listen(textarea, 'focusout', () => {
                this.render.removeClass(formNode, 'active-btn');
            });

            this.render.listen(button, 'click', () => {
                this.render.setStyle(refChild, 'display', 'block');
                this.render.removeChild(parentNode, formNode);

                // let author_email = '';
                // let content = '';
                // let post = '';
                // let parent = '';
                // let author_url = '';

                // this.http.API_ENDPOINT = '&author_email=' + author_email + '&content=' + content + '&post=' + post + '&parent=' + parent + '&author_url=' + author_url; console.log(this.http.API_ENDPOINT);
                // this.http.postDataComment().subscribe();
            });
        });
    }
}