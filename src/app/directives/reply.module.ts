import { NgModule } from "@angular/core";

import { ReplyDirective } from './reply.directive';

@NgModule({
    declarations: [ReplyDirective],
    exports: [ReplyDirective]
})

export class ReplyModule { }
