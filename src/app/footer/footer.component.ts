import { Component, OnInit } from '@angular/core';
import { VariableService } from '../caller/variable.service';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html'
})

export class FooterComponent implements OnInit {

    loading: boolean = false;

    constructor(private loader: VariableService) { }

    ngOnInit() {
        this.loader.loadState.subscribe(res => this.loading = res);
    }

}
