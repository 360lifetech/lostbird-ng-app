import { Observable } from "rxjs";
import { AuthConfig } from 'angular-oauth2-oidc';

// export var server: string = 'https://beta.lostbird.vn/wp-json/'; 
export var server: string = 'https://lostbird.local/wp-json/'; 
export var api: string = 'lostbird/v1'

export const authConfig: AuthConfig = {
    issuer: 'https://id.360life.cf/auth/realms/master',
    redirectUri: window.location.origin + '/users',
    clientId: 'lostbird',
    // dummyClientSecret: 'lostbird',

    scope: 'openid profile email',
    showDebugInformation: true,
    sessionChecksEnabled: false
}

export interface LoaderState {
    show: boolean
}

export interface MoreState {
    show: boolean
}

export class localStore {
    id: number;
    count: number;
}

export class Cate {
    id: number;
    term_id: number;
    name: string;
    slug: string;
    description: string;
    count: number;
    link: string;
    image: string;
}

export class Author {
    name: string;
    slug: string;
    description: string;
}

export class Post {
    id: string;
    title: string;
    date: string;
    image: string;
    cate: Cate;
    source: string;
    content: string;
    excerpt: string;
    author: Author;
    tags: Tags;
    more: SingleRelated;
    good: SingleRelated;
    serie: boolean;
    hadpost: string;
}

export class Posts {
    id: number;
    title: string;
    link: string;
    image: string;
    view: number;
    date: string;
    daytotime: number;
    skull: boolean;
    excerpt: string;
    cate: Cate;
    author: Author;
}

export class Related {
    posts: Observable<Posts[]>;
    hadpost: string;
}

export class NewPost {
    posts: Object;
    name: string;
    count: number;
    hadpost: string;
    page: number;
    maxPage: number;
}

export class Tags {
    name: string;
    slug: string;
    description: string;
}

export class SingleRelated {
    title: string;
    link: string;
    skull: boolean;
    image: string;
}

export class Menu {
    id: number;
    slug: string;
    title: string;
    child: Observable<Menu[]>;;
}

export class Comment {
    author_email: string;
    author_url: string;
    author_name: string;
    content: string;
    post: string;
    parent: number;
}
