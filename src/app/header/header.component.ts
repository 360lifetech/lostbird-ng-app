import { Component, OnInit, Inject, PLATFORM_ID, HostListener } from '@angular/core'
import { Router, NavigationEnd } from '@angular/router'
import { Location, isPlatformBrowser } from '@angular/common'

import { OAuthService } from 'angular-oauth2-oidc'

import { HttpService } from '.././caller/http.service'
import { CacheService } from '.././caller/cache.service'
import { MenuService } from '../caller/menu.service'
import { LocalService } from '../caller/local.service'
import { VariableService } from '../caller/variable.service'

import { Posts } from '../globals'

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})

export class HeaderComponent implements OnInit {
    user: object;
    showLog: boolean;
    linkHis: string;
    unread: number[];
    post: string[] = [];

    index: boolean = true;
    single: boolean = false;

    slug: string;
    menus: object = [];
    catecount: object = [];
    hotpost: Posts[];
    radomPost: Posts;
    data: any;

    message: boolean;
    random: boolean;
    navbar: boolean = false;

    private isBrowser;
    private detect: number;

    constructor(
        private location: Location,
        private router: Router,
        private val: VariableService,
        private oauthService: OAuthService,
        private singleService: HttpService,
        private cacheService: CacheService,
        private menuService: MenuService,
        private localService: LocalService,
        @Inject(PLATFORM_ID) private platformId: object
    ) {
        this.isBrowser = isPlatformBrowser(this.platformId);
    }

    @HostListener('window:resize', ['$event']) onResize(event) {
        if (this.single) {
            let w = event.target.innerWidth;
            if (w <= 360) this.index = true; else this.index = false;
        }
    }

    @HostListener("window:scroll", ['$event']) onWindowScroll(e) {
        if (this.detect > window.scrollY)
            this.navbar = false;
        else
            if (window.scrollY > window.innerHeight)
                this.navbar = true;
            else
                this.navbar = false;

        this.detect = window.scrollY;
    }

    public login() {
        this.oauthService.initImplicitFlow();
    }

    public logout() {
        this.oauthService.logOut();
    }

    hideAlert() {
        this.val.changeRandomState(false);
    }

    ngOnInit() {
        this.router.events.subscribe(e => {
            if (e instanceof NavigationEnd) {
                switch (this.location.path().toString()) {
                    case '':
                        this.index = true;
                        this.single = false;
                        break;
                    default:
                        this.index = false;
                        this.single = true;
                        break;
                }

                if (this.isBrowser) {
                    this.user = this.oauthService.getIdentityClaims(); console.log(this.user);
                    this.showLog = this.user != null ? true : false;
                    this.post = this.localService.findAll('lostbird-post');
                    if (this.single && window.screen.width <= 360) this.index = true;
                }
            }
        });

        this.menus = this.menuService.getMenu();

        this.val.randomState.subscribe((random) => { this.random = random });

        this.getCategoryCount();
    }

    getRandomPost() {
        let hadPost = (this.post.length == 0) ? 1 : this.post.join('-');
        this.singleService.API_ENDPOINT = '/randompost/' + hadPost;
        this.cacheService.get(this.singleService.API_ENDPOINT, this.singleService.getPost()).subscribe(
            data => {
                this.radomPost = data[0];
                this.val.changeScrollState(true);
                this.router.navigate([this.radomPost.link]);
            }
        );
    }

    getHostPosts() {
        this.singleService.API_ENDPOINT = '/hotposts/';
        this.cacheService.get(this.singleService.API_ENDPOINT, this.singleService.getPosts()).subscribe(
            data => {
                this.hotpost = data;
                this.unread = this.findNewPost(this.hotpost);
            }
        );
    }

    findNewPost(hotpost: Posts[]): number[] {
        if (hotpost.length == 0) return [];

        let unread: number[] = [];
        for (var i = 0; i < 3; i++) {
            let highest: Posts = hotpost.reduce((max, current) => (current.daytotime > max.daytotime) ? current : max);
            unread.push(highest.id);
            hotpost = hotpost.filter(function (obj) { return obj.id !== highest.id });
        }
        return unread;
    }

    getCategoryCount() {
        this.singleService.API_ENDPOINT = '/category_count/';
        this.cacheService.get(this.singleService.API_ENDPOINT, this.singleService.getData()).subscribe(
            data => {
                this.catecount = data;
            }
        );
    }

    goSearch(formSearch) {
        let key = formSearch.value.search;
        this.router.navigate(['/tim-kiem', key]);
    }
}
