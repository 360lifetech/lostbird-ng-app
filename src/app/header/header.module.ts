import { NgModule } from "@angular/core"
import { FormsModule } from "@angular/forms"
import { CommonModule } from "@angular/common"
import { BrowserModule } from "@angular/platform-browser"

import { HeaderComponent } from "./header.component"
import { AppShellModule } from "../directives/appShell.module";
import { AppRoutingModule } from "../app-routing.module"
import { UsersModule } from "../users/users.module"
import { ModalModule } from "../directives/modal.module"

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, FormsModule, AppRoutingModule, BrowserModule, UsersModule, AppShellModule, ModalModule],
  exports: [HeaderComponent]
})

export class HeaderModule { }
