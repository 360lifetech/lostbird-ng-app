import { Title, Meta } from '@angular/platform-browser'
import { Component, OnInit, HostListener, Inject, PLATFORM_ID } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'

import { HttpService } from '../caller/http.service'
import { CacheService } from '../caller/cache.service'
import { LocalService } from '../caller/local.service'
import { Subscription } from 'rxjs'

import { Posts, NewPost, Post } from '../globals'
import { isPlatformBrowser } from '@angular/common'

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html'
})

export class IndexComponent implements OnInit {

    fisrtload: boolean = true;

    SEO = new Post;
    posts;

    featured: Posts;
    m_giai_tri: Posts;
    m_kham_pha: Posts;
    m_song_dep: Posts;
    m_song_deep: Posts;

    newpost: NewPost;
    news: Object;

    hadPost: String = '1';
    page: number = 0;
    maxPage: number = 1;
    flag: boolean = true;
    more: boolean = false;

    schema: Object;
    infomationSEO: Object = {};

    private load: Subscription;
    private isBrowser;

    constructor(
        private singleService: HttpService,
        private cacheService: CacheService,
        private localService: LocalService,
        private route: ActivatedRoute,
        private router: Router,
        private title: Title,
        private meta: Meta,
        @Inject(PLATFORM_ID) private platformId: object
    ) {
        this.isBrowser = isPlatformBrowser(this.platformId);

        if (this.isBrowser) {
            this.localService.add(this.router.url, 'lostbird-history');
            window.scrollTo(0, 0);
        }
    }

    @HostListener('window:scroll', ['$event']) scrollHandler() {
        if (window.innerHeight + window.scrollY > window.innerHeight * 2) {
            if (window.innerHeight + window.scrollY >= document.querySelector('.wrapper').clientHeight - 400 && this.flag) {
                this.flag = false;
                this.loadMore();
            }
        }
    }

    ngOnInit() {
        this.featured = this.route.snapshot.data['index'].posts;;
        this.hadPost = this.route.snapshot.data['index'].hadpost.join('-');

        this.SEO.title = 'Lost Bird - Web Lành Chim Đậu';
        this.SEO.excerpt = 'Trang thông tin bổ ích dành cho giới trẻ. Mái nhà dành cho những cánh chym đang lạc lối giữa bão thông tin thị phi trên mạng xã hội ngày nay.';
        this.SEO.image = 'http://img.lostbird.vn/2017/11/01/20171031_logolostbird_1024x1024.png';

        this.setSEO(this.SEO, this.title, this.meta);

        this.singleService.API_ENDPOINT = '/index/' + this.hadPost;
        this.load = this.cacheService.get(this.singleService.API_ENDPOINT, this.singleService.getData()).subscribe(
            data => {
                this.posts = data;
                this.hadPost = data.hadPost.join('-');
            }
        );

        this.loadMore();
    }

    loadMore() {
        if (this.page != this.maxPage) {
            this.singleService.API_ENDPOINT = '/more/' + this.hadPost + '/' + this.page;
            this.load = this.singleService.getNewPost().subscribe(
                data => {
                    this.newpost = data;
                    if (this.more) {
                        this.news = [].concat(this.news, this.newpost.posts);
                    } else {
                        this.news = this.newpost.posts;
                        this.more = true;
                    }
                    this.maxPage = data.maxPage;
                    this.page += 1;
                }
            );
        }
    }

    setSEO(data: Post, title: Title, meta: Meta) {
        title.setTitle(data.title);

        meta.updateTag({ name: 'description', content: data.excerpt });
        meta.updateTag({ name: 'url', content: this.router.url });
        meta.updateTag({ name: 'og:image', content: data.image });
        meta.updateTag({ name: 'og:site_name', content: 'Lost Bird' });
        meta.updateTag({ name: 'twitter:image', content: data.image });
        meta.updateTag({ property: 'og:Title', content: data.title });
        meta.updateTag({ property: 'og:description', content: data.excerpt });
        meta.updateTag({ property: 'og:url', content: this.router.url });
        meta.updateTag({ property: 'og:type', content: 'website' });
        meta.updateTag({ property: 'twitter:title', content: data.title });
        meta.updateTag({ property: 'twitter:description', content: data.excerpt });
        meta.updateTag({ property: 'twitter:card', content: data.image });

        this.schema = {
            "@context": "http://schema.org",
            "@type": "Organization",
            "url": this.router.url,
            "name": data.title
        };
    }
}
