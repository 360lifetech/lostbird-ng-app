import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgxJsonLdModule } from "@ngx-lite/json-ld";

import { LoaderModule } from "../caller/loader/loader.module";
import { MenuchildModule } from "../menuchild/menuchild.module";
import { AppShellModule } from "../directives/appShell.module";
import { PushInModule } from "../directives/pushin.module";

import { IndexComponent } from "./index.component";

@NgModule({
    declarations: [IndexComponent],
    imports: [CommonModule, LoaderModule, RouterModule, MenuchildModule, NgxJsonLdModule, AppShellModule, PushInModule]
})

export class IndexModule { }
