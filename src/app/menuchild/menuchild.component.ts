import { Component, OnInit, Input} from '@angular/core';

import { MenuService } from '../caller/menu.service';
import { Menu, Posts } from '../globals';

@Component({
    selector: 'app-menuchild',
    templateUrl: './menuchild.component.html'
})

export class MenuchildComponent implements OnInit {

    load: boolean = false;
    menuContent: Menu;

    @Input() menu: number;
    @Input() content: Posts;

    constructor(
        private menuService: MenuService
    ) { }

    ngOnInit() {
        this.menuContent = this.menuService.getMenuChild(this.menu);
    }
}
