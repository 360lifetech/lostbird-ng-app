import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MenuchildComponent } from './menuchild.component';
import { CommonModule } from "@angular/common";
import { PushInModule } from '../directives/pushin.module'
import { ModalModule } from "../directives/modal.module";

@NgModule({
  declarations: [MenuchildComponent],
  imports: [RouterModule, CommonModule, PushInModule, ModalModule],
  exports: [MenuchildComponent],
})

export class MenuchildModule { }
