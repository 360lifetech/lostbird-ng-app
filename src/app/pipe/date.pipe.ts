import { PipeTransform, Pipe } from "@angular/core";

@Pipe({ name: 'dateCountdown' })

export class DateCountdown implements PipeTransform {
    transform(value: any): string {
        var dateNames = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"];
        var monthNames = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

        let today = new Date();
        let post_day = new Date(value);
        var sec_num = Math.floor((today.getTime() - post_day.getTime()) / 1000);
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        if (hours > 24)
            return post_day.getHours() + ':' + post_day.getMinutes() + ' | ' + dateNames[post_day.getDate()] + '/' + monthNames[post_day.getMonth()] + '/' + post_day.getFullYear();
        else if (hours > 0 && hours <= 24)
            return hours + ' giờ trước';
        else
            return minutes + ' phút trước';
    }
}

