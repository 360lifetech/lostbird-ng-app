import { PipeTransform, Pipe } from "@angular/core";

@Pipe({ name: 'authorFollow' })

export class AuthorFollow implements PipeTransform {
    transform(value: string): string {
        return 'Theo: ' + value;
    }
}

