import { Title, Meta } from "@angular/platform-browser";
import { Component, OnInit, HostListener, Inject, PLATFORM_ID } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { isPlatformBrowser } from "@angular/common";
import { Subscription } from "rxjs";

import { HttpService } from ".././caller/http.service";
import { CacheService } from ".././caller/cache.service";
import { LocalService } from "../caller/local.service";

import { NewPost, Cate } from "../globals";

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html'
})

export class PostsComponent implements OnInit {
    slug: string;
    headImage: string = 'http://placehold.it/720x340/fffdf4/333333?text=+';

    cate: Cate;
    newpost: NewPost;
    news: Object;

    page: number = 0;
    maxPage: number = 1;
    flag: boolean = true;
    more: boolean = false;

    loading: boolean = false;

    schema: Object;
    infomationSEO: Object;

    private load: Subscription;
    private isBrowser;

    constructor(
        private singleService: HttpService,
        private cacheService: CacheService,
        private localService: LocalService,
        private route: ActivatedRoute,
        private router: Router,
        private title: Title,
        private meta: Meta,
        @Inject(PLATFORM_ID) private platformId: object
    ) {
        this.isBrowser = isPlatformBrowser(this.platformId);
    }

    @HostListener("window:scroll", []) onWindowScroll() {
        if (window.innerHeight + window.scrollY > window.innerHeight * 2) {
            if (window.innerHeight + window.scrollY >= document.querySelector('.wrapper').clientHeight - 400 && this.flag) {
                this.flag = false;
                this.loadMore();
            }
        }
    }

    ngOnInit() {
        this.setSEO(this.route.snapshot.data['tax'], this.title, this.meta);

        this.route.paramMap.subscribe(() => {
            this.cate = this.route.snapshot.data['tax'];
            this.headImage = this.cate.image;
            this.reset();
            this.loadMore();

            if (this.isBrowser) {
                window.scrollTo(0, 0);
                this.localService.add(this.cate.term_id, 'lostbird-cate');
                this.localService.add(this.router.url, 'lostbird-history');
            }
        })
    }

    loadMore() {
        if (this.page != this.maxPage) {
            this.singleService.API_ENDPOINT = '/archive/' + this.cate.slug + '/' + this.page;
            this.load = this.cacheService.get(this.singleService.API_ENDPOINT, this.singleService.getNewPost()).subscribe(
                data => {
                    this.newpost = data;
                    if (this.more) {
                        this.news = [].concat(this.news, this.newpost.posts);
                    } else {
                        this.news = this.newpost.posts;
                        this.more = true;
                    }
                    this.maxPage = data.maxPage;
                    this.page += 1;
                }
            );
        }
    }

    reset() {
        this.flag = true;
        this.more = false;
        this.page = 0;
        this.maxPage = 1;
    }

    setSEO(data: Cate, title: Title, meta: Meta) {
        title.setTitle(data.name);

        meta.updateTag({ name: 'description', content: data.description });
        meta.updateTag({ name: 'url', content: this.router.url });
        meta.updateTag({ name: 'og:image', content: data.image });
        meta.updateTag({ name: 'twitter:image', content: data.image });
        meta.updateTag({ property: 'og:Title', content: data.name });
        meta.updateTag({ property: 'og:description', content: data.description });
        meta.updateTag({ property: 'og:url', content: this.router.url });
        meta.updateTag({ property: 'og:type', content: 'website' });
        meta.updateTag({ property: 'twitter:title', content: data.name });
        meta.updateTag({ property: 'twitter:description', content: data.description });
        meta.updateTag({ property: 'twitter:card', content: data.image });

        this.schema = {
            "@context": "http://schema.org",
            "@type": "Organization",
            "url": this.router.url,
            "name": data.name
        };
    }
}
