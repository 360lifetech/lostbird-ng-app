import { Component, OnInit, HostListener, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { HttpService } from '../caller/http.service';
import { CacheService } from '../caller/cache.service';
import { LocalService } from '../caller/local.service';
import { NewPost } from '../globals';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html'
})

export class SearchComponent implements OnInit {
    slug: string;
    searchPost: NewPost;
    news: Object;

    page: number = 0;
    maxPage: number = 1;
    flag: boolean = true;
    more: boolean = false;

    private load: Subscription;
    private isBrowser;

    constructor(
        private singleService: HttpService,
        private cacheService: CacheService,
        private localService: LocalService,
        private route: ActivatedRoute,
        private router: Router,
        @Inject(PLATFORM_ID) private platformId: object
    ) {
        this.isBrowser = isPlatformBrowser(this.platformId);
    }

    @HostListener("window:scroll", []) onWindowScroll() {
        if (window.innerHeight + window.scrollY > window.innerHeight * 2) {
            if (window.innerHeight + window.scrollY >= document.querySelector('.wrapper').clientHeight - 400 && this.flag) {
                this.flag = false;
                this.loadMore();
            }
        }
    }

    ngOnInit() { 
        this.route.paramMap.subscribe(() => {
            this.reset();
            this.loadMore();

            if (this.isBrowser) {
                window.scrollTo(0, 0);
                this.localService.add(this.router.url, 'lostbird-history');
            }
        })
    }

    loadMore() {
        if (this.page != this.maxPage) {
            this.slug = this.route.snapshot.params['slug'];
            this.singleService.API_ENDPOINT = '/searchs/' + this.unicodeChange(this.slug) + '/' + this.page;
            this.load = this.cacheService.get(this.singleService.API_ENDPOINT, this.singleService.getNewPost()).subscribe(
                data => {
                    this.searchPost = data;
                    if (this.more) {
                        this.news = [].concat(this.news, this.searchPost.posts);
                    } else {
                        this.news = this.searchPost.posts;
                        this.more = true;
                    }
                    this.maxPage = data.maxPage;
                    this.page += 1;
                }
            );
        }
    }

    goSearchOnPage(formSearchOnPage) {
        let key = formSearchOnPage.value.search;
        this.slug = key;
        this.router.navigate(['/tim-kiem', key]);
    }

    removeKey() {
        this.slug = '';
    }

    reset() {
        window.scroll(0, 0);
        this.flag = true
        this.more = false;
        this.page = 0;
        this.maxPage = 1;
    }

    unicodeChange(str: string) {
        if (str != null) {
            str = str.toLowerCase();
            str = str.replace(/\%20/g, '-');
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
            str = str.replace(/-+-/g, "-");
            str = str.replace(/^\-+|\-+$/g, "");
        }
        return str;
    }
}
