import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { NgxJsonLdModule } from "@ngx-lite/json-ld";

import { LoaderModule } from "../caller/loader/loader.module";
import { SearchComponent } from "./search.component";

const routes: Routes = [
    {
        path: ":slug",
        component: SearchComponent
    }
];

@NgModule({
    declarations: [SearchComponent],
    imports: [CommonModule, RouterModule.forChild(routes), LoaderModule, FormsModule, NgxJsonLdModule]
})

export class SearchModule { }
