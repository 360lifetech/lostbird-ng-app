import { Title, Meta } from '@angular/platform-browser';
import { Component, OnInit, HostListener, Inject, PLATFORM_ID, ViewChild, ElementRef } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

import { HttpService } from '../caller/http.service';
import { CacheService } from '../caller/cache.service';
import { LocalService } from '../caller/local.service';
import { EmbedService } from '../caller/embed.service';
import { VariableService } from '../caller/variable.service'
import { Post, Related, NewPost, Posts, Comment } from '../globals';

@Component({
    selector: 'app-single',
    templateUrl: './single.component.html',
    providers: [HttpService]
})

export class SingleComponent implements OnInit {
    share: string;
    headImage: string;

    post: Post;

    posttag: Related;
    related: Related;
    newpost: NewPost;

    news: Object;

    hadPost: string;
    page: number = 0;
    maxPage: number = 1;
    flag: boolean = true;
    more: boolean = false;

    // SEO infomation
    schema: object;

    // Detect
    lastOffset: number = 0;
    lastDate = new Date().getTime();
    randomPost: Posts;

    // localStore
    private cateLocal: string;
    private postLocal: string;
    private isBrowser: boolean;

    // Effect
    private navbar: boolean = false;

    // Series
    seriesHis: Post[] = [];
    series: Posts[] = [];
    seriesNext: Posts;
    seriesFlag: boolean = true;
    seriesShow: boolean = false;
    seriesEnd: boolean = false;
    seriesAlert: string;

    // Comment
    cLogged: boolean = true;
    cRawData: Object[];
    cGone: Object[];
    cFirst: boolean;
    cMore: boolean = true;
    cOffset: number = 0;
    cPerpage: number = 21;


    detect: number;
    scrollTop: boolean = true;
    load: Subscription;

    constructor(
        private httpService: HttpService,
        private cacheService: CacheService,
        private localService: LocalService,
        private oauthService: OAuthService,
        private value: VariableService,
        private embed: EmbedService,
        private route: ActivatedRoute,
        private router: Router,
        private title: Title,
        private meta: Meta,
        @Inject(PLATFORM_ID) private platformId: object
    ) {
        this.isBrowser = isPlatformBrowser(this.platformId);
    }

    @ViewChild('singleContent') elementView: ElementRef;

    @HostListener("window:scroll", ['$event']) onWindowScroll(e) {

        if (this.detect < window.scrollY) {
            const c = this.elementView.nativeElement.offsetHeight;
            const t = this.elementView.nativeElement.offsetTop;
            const h = c + t;

            // Detect speed scroll
            if (window.scrollY < h && this.localService.findAll('lostbird-postrand').length == 0) {
                let delayInMs = e.timeStamp - this.lastDate;
                let offset = window.scrollY - this.lastOffset;
                let detect = offset / delayInMs;
                if (detect > 8) {
                    this.value.changeRandomState(true);
                    this.localService.add(true, 'lostbird-postrand');
                }
                this.lastDate = e.timeStamp;
                this.lastOffset = window.scrollY;
            }

            // Load more when scroll bottom
            if (window.innerHeight + window.scrollY > window.innerHeight * 2) {
                if (window.innerHeight + window.scrollY >= document.querySelector('.wrapper').clientHeight - 400 && this.flag) {
                    this.flag = false;
                    if (this.seriesNext == null)
                        this.loadMore();
                }
            }

            // Load more series
            if (window.scrollY > h - (window.innerHeight * 3) && this.seriesFlag && this.post.serie !== null) {
                this.seriesFlag = false;
                if (this.seriesNext !== null) {
                    this.router.navigate([this.seriesNext.link]);
                    this.scrollTop = false;
                }
            }

        }

        // Show / Hide sidebar bottom
        if (window.scrollY > window.innerHeight) this.navbar = true; else this.navbar = false;

        this.detect = window.scrollY;
    }

    ngOnInit(): void {
        this.share = 'https://www.facebook.com/sharer/sharer.php?u=https://lostbird.vn/' + this.router.url;

        this.route.paramMap.subscribe(() => {
            this.reset();
            this.post = this.route.snapshot.data['post'];
            this.setSEO(this.post, this.title, this.meta);
            this.hadPost = this.post.hadpost;
            this.headImage = this.post.image;

            this.cGone = [];
            this.getComments();
            this.value.scrollState.subscribe(value => this.scrollTop = value);

            if (this.isBrowser && this.scrollTop) {
                window.scrollTo(0, 0);
                this.seriesHis = [];
            }

            this.seriesHis.push(this.post);

            if (this.isBrowser) {
                setTimeout(() => { this.localService.add(this.post.cate.id, 'lostbird-cate'); }, 20000);

                this.localService.add(this.post.id, 'lostbird-post');
                this.localService.add(this.router.url, 'lostbird-history');

                this.cateLocal = this.localService.findMax('lostbird-cate').length === 0 ? this.post.cate.id + '' : this.localService.findMax('lostbird-cate').join('-');
                this.postLocal = this.localService.findAll('lostbird-post').join('-');

                if (this.post.serie !== null)
                    this.getPostSeries();

                this.getPostRelated();
                this.getPostSuggest();
                this.getPostTags();

                this.embed.embedInstagram();
                this.embed.embedFacebook();
                this.embed.embedTwitter();
            }
        });
    }

    commentSubmit(f: NgForm) {
        // this.oauthService.initImplicitFlow();
        console.log(f.value.commentContent);

        let data: Comment = new Comment;
        data.author_email = 'minh.vo2@360life.vn';
        data.author_url = '';
        data.author_name = 'Minh Vo 2',
        data.content = f.value.commentContent;
        data.post = this.post.id;
        data.parent = 0;

        // let author_email = 'minh.vo@360life.vn';
        // let author_url = '';
        // let content = f.value.commentContent;
        // let post = this.post.id;
        // let parent = 0;

        // this.httpService.API_ENDPOINT = '&author_email=' + author_email + '&content=' + content + '&post=' + post + '&parent=' + parent + '&author_url=' + author_url;
        this.httpService.postDataComment(data).subscribe();
    }

    destroySeries() {
        this.scrollTop = true;
    }

    private getComments() {
        this.httpService.API_ENDPOINT = '/comments?post=' + this.post.id + '&orderby=id&per_page=' + this.cPerpage + '&offset=' + this.cOffset;
        this.cacheService.get(this.httpService.API_ENDPOINT, this.httpService.getDataComment()).subscribe(
            data => {
                if (this.cOffset === 0) this.cRawData = data; else { this.cRawData.splice(2, 1); this.cRawData = [].concat(this.cRawData, data); }
                if (data.length != this.cPerpage) this.cMore = false; else this.cOffset = this.cOffset - 1 + this.cPerpage;
                if (data.length == 0) this.cFirst = true;
                else {
                    this.cFirst = false;
                    this.cGone = this.cRawData.filter(s => s['parent'] == 0);
                    this.cGone.forEach(element => {
                        let child = this.cRawData.filter(s => s['parent'] == element['id']);
                        if (child.length !== 0)
                            element['child'] = child;
                    });
                }
            }
        );
    }

    private getRandomPost() {
        this.httpService.API_ENDPOINT = '/randompost/' + this.postLocal;
        this.cacheService.get(this.httpService.API_ENDPOINT, this.httpService.getPost()).subscribe(
            data => {
                this.router.navigate([data[0].link]);
            }
        );
    }

    private getPostSuggest() {
        this.httpService.API_ENDPOINT = '/postsuggest/' + this.postLocal + '/' + this.cateLocal;
        this.load = this.cacheService.get(this.httpService.API_ENDPOINT, this.httpService.getRelated()).subscribe(
            data => {
                this.hadPost = data.hadpost.join('-');
                this.posttag = data;
            }
        );
    }

    private getPostTags() {
        this.httpService.API_ENDPOINT = '/poststags/' + this.hadPost + '/' + this.post.id;
        this.load = this.cacheService.get(this.httpService.API_ENDPOINT, this.httpService.getRelated()).subscribe(
            data => {
                this.hadPost = data.hadpost.join('-');
                this.posttag = data;
                this.loadMore();
            }
        );
    }

    private getPostRelated() {
        this.httpService.API_ENDPOINT = '/related/' + this.hadPost + '/' + this.post.cate.id;
        this.load = this.cacheService.get(this.httpService.API_ENDPOINT, this.httpService.getRelated()).subscribe(
            data => {
                this.hadPost = data.hadpost.join('-');
                this.related = data;
            }
        );
    }

    private getPostSeries() {
        this.httpService.API_ENDPOINT = '/serie/' + this.post.id;
        this.load = this.cacheService.get('series-' + this.post.serie, this.httpService.getRelated()).subscribe(
            data => {
                this.series = data;
                this.getNextPostSeries(this.series, parseInt(this.post.id));
            }
        );
    }

    private getNextPostSeries(series: Posts[], id: number): void {
        let index = series.findIndex(row => row.id === id);
        if (index < series.length - 1)
            this.seriesNext = this.series[index + 1]
        else {
            this.seriesNext = null;
            this.seriesAlert = 'Bạn đang xem bài cuối cùng của series';
        }
    }

    private loadMore() {
        if (this.page != this.maxPage) {
            this.httpService.API_ENDPOINT = '/more/' + this.hadPost + '/' + this.page;
            this.load = this.httpService.getNewPost().subscribe(
                data => {
                    this.newpost = data;
                    if (this.more) {
                        this.news = [].concat(this.news, this.newpost.posts);
                    } else {
                        this.news = this.newpost.posts;
                        this.more = true;
                    }
                    this.maxPage = data.maxPage;
                    this.page += 1;
                }
            );
        }
    }

    private setSEO(data: Post, title: Title, meta: Meta) {
        title.setTitle(data.title);

        meta.updateTag({ name: 'description', content: data.excerpt });
        meta.updateTag({ name: 'url', content: this.router.url });
        meta.updateTag({ name: 'og:image', content: data.image });
        meta.updateTag({ name: 'twitter:image', content: data.image });
        meta.updateTag({ property: 'og:Title', content: data.title });
        meta.updateTag({ property: 'og:description', content: data.excerpt });
        meta.updateTag({ property: 'og:url', content: this.router.url });
        meta.updateTag({ property: 'og:type', content: 'website' });
        meta.updateTag({ property: 'twitter:title', content: data.title });
        meta.updateTag({ property: 'twitter:description', content: data.excerpt });
        meta.updateTag({ property: 'twitter:card', content: data.image });

        this.schema = {
            "@context": "http://schema.org",
            "@type": "Organization",
            "url": this.router.url,
            "name": data.title
        };
    }

    private reset() {
        this.more = false;
        this.flag = true;
        this.seriesFlag = true;
        this.page = 0;
        this.maxPage = 1;
    }
}
