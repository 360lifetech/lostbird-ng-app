import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgxJsonLdModule } from "@ngx-lite/json-ld";
import { RouterModule, Routes, UrlSegment } from "@angular/router";

import { SingleComponent } from "./single.component";
import { PostsComponent } from "../posts/posts.component";

import { LoaderModule } from "../caller/loader/loader.module";
import { AppShellModule } from "../directives/appShell.module";
import { PushInModule } from "../directives/pushin.module";
import { ModalModule } from "../directives/modal.module"
import { ReplyModule } from "../directives/reply.module"

import { SafeHtmlPipe } from "../pipe/safe.pipe";
import { AuthorFollow } from "../pipe/source.pipe";
import { DateCountdown } from "../pipe/date.pipe";

import { PostResolver } from "../caller/post.resolver";
import { TaxResolver } from "../caller/tax.resolver";

const routes: Routes = [
    {
        path: ":slug",
        component: PostsComponent,
        resolve: {
            tax: TaxResolver
        }
    },
    {
        matcher: htmlUrl,
        component: SingleComponent,
        resolve: {
            post: PostResolver
        }
    },
    {
        path: ":parent/:archive/:slug",
        component: SingleComponent,
        resolve: {
            post: PostResolver
        }
    },
    {
        path: ":parent/:slug",
        component: PostsComponent,
        resolve: {
            tax: TaxResolver
        }
    },
];

export function htmlUrl(url: UrlSegment[]) {
    return url[1].path.endsWith(".html") ? ({ consumed: url }) : null;
}


@NgModule({
    declarations: [SingleComponent, PostsComponent, SafeHtmlPipe, AuthorFollow, DateCountdown],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LoaderModule, NgxJsonLdModule, AppShellModule, PushInModule, ModalModule, ReplyModule, FormsModule
    ],
    providers: [PostResolver, TaxResolver],
})

export class SingleModule { }
