import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { NgxJsonLdModule } from "@ngx-lite/json-ld";

import { LoaderModule } from "../caller/loader/loader.module";

import { TagsComponent } from "./tags.component";
import { TaxResolver } from "../caller/tax.resolver";

const routes: Routes = [
    {
        path: ":slug",
        component: TagsComponent,
        resolve: {
            tax: TaxResolver
        }
    }
];

@NgModule({
    declarations: [TagsComponent],
    imports: [CommonModule, RouterModule.forChild(routes), LoaderModule, NgxJsonLdModule],
    providers: [TaxResolver]
})

export class TagsModule { }
