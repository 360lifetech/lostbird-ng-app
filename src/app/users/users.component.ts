import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { LocalService } from '../caller/local.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html'
})

export class UsersComponent implements OnInit {

    private isBrowser;

    constructor(
        private router: Router,
        private localService: LocalService,
        @Inject(PLATFORM_ID) private platformId: object
    ) {
        this.isBrowser = isPlatformBrowser(this.platformId);
    }

    ngOnInit() {
        if (this.isBrowser) {
            let link: Object = this.localService.findAll('lostbird-history');
            this.router.navigate([link[0]['link']]);
        }
    }
}
