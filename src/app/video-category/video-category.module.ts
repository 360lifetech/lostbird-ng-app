import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { NgxJsonLdModule } from "@ngx-lite/json-ld";

import { LoaderModule } from "../caller/loader/loader.module";
import { TaxResolver } from "../caller/tax.resolver";
import { ModalModule } from "../directives/modal.module";

import { VideoCategoryComponent } from "./video-category.component";

const routes: Routes = [
    {
        path: ":slug",
        component: VideoCategoryComponent,
        resolve: {
            tax: TaxResolver
        }
    }
];

@NgModule({
    declarations: [VideoCategoryComponent],
    imports: [CommonModule, RouterModule.forChild(routes), LoaderModule, NgxJsonLdModule, ModalModule],
    providers: [TaxResolver]
})

export class VideoCategoryModule { }
