import { Title, Meta } from '@angular/platform-browser';
import { Component, OnInit, HostListener, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { HttpService } from '../caller/http.service';
import { MenuService } from '../caller/menu.service';
import { LocalService } from '../caller/local.service';
import { Post, NewPost } from '../globals';

declare const MediaElementPlayer: any;

@Component({
    selector: 'app-video-detail',
    templateUrl: './video-detail.component.html'
})

export class VideoDetailComponent implements OnInit {
    slug: string;
    song: string = '../../assets/images/videoplayback.3gp';

    private player;

    video: Post;
    newpost: NewPost;
    news: object;
    menu: Object;

    hadpost: string;
    page: number = 0;
    maxPage: number = 1;
    flag: boolean = true;
    more: boolean = false;
    check: boolean = true;

    schema: object;

    private load: Subscription;
    private isBrowser;

    constructor(
        private singleService: HttpService,
        private menuService: MenuService,
        private route: ActivatedRoute,
        private localService: LocalService,
        private router: Router,
        private title: Title,
        private meta: Meta,
        @Inject(PLATFORM_ID) private platformId: object
    ) {
        this.isBrowser = isPlatformBrowser(this.platformId);
    }

    @HostListener("window:scroll", []) onWindowScroll() {
        if (window.innerHeight + window.scrollY > window.innerHeight * 2) {
            if (window.innerHeight + window.scrollY >= document.querySelector('.wrapper').clientHeight - 400 && this.flag) {
                this.flag = false;
                this.loadMore();
            }
        }
    }

    ngOnInit() {
        this.setSEO(this.route.snapshot.data['video'], this.title, this.meta);

        this.route.paramMap.subscribe(() => {
            if (this.isBrowser) { 
                window.scrollTo(0, 0);
                this.localService.add(this.router.url, 'lostbird-history'); 
            }
            this.reset();
            this.slug = this.route.snapshot.params['slug'];
            this.video = this.route.snapshot.data['video'];
            this.hadpost = this.video.id + '';
            this.song = /(https?:\/\/[^\s]+.mp4)/g.exec(this.video.content)[0];

            if (this.player !== undefined) {
                this.player.setSrc(this.song);
                this.player.load();
            }

            this.loadMore();
        })

        this.menu = this.menuService.getMenuChild(0)['child'];
        if (this.player === undefined) this.videoInit();
        this.player.setSrc(this.song);
        this.player.load();
    }


    videoInit() {
        this.player = new MediaElementPlayer('player', {
            videoWidth: "100%",
            videoHeight: "100%",
            features: ["playpause", "progress", "current", "fullscreen", "sourcechooser"],
            success: function (media, node, player) {
                player.container.addEventListener('mouseenter', function () {
                    if (player.controlsEnabled) {
                        player.killControlsTimer('enter');
                        player.showControls();
                        player.startControlsTimer(player.options.controlsTimeoutMouseEnter);
                    }
                });
                player.container.addEventListener('mousemove', function () {
                    if (player.controlsEnabled) {
                        if (!player.controlsAreVisible) {
                            player.showControls();
                        }
                        player.startControlsTimer(player.options.controlsTimeoutMouseEnter);
                    }
                });
                player.container.addEventListener('mouseleave', function () {
                    if (player.controlsEnabled) {
                        if (!player.paused) {
                            player.startControlsTimer(player.options.controlsTimeoutMouseLeave);
                        }
                    }
                });
            }
        });
    }

    loadMore() {
        if (this.page != this.maxPage) {
            this.singleService.API_ENDPOINT = '/videodetailmore/' + this.hadpost + '/' + this.page;
            this.load = this.singleService.getNewPost().subscribe(
                data => {
                    this.newpost = data;
                    if (this.more) {
                        this.news = [].concat(this.news, this.newpost.posts);
                    } else {
                        this.news = this.newpost.posts;
                        this.more = true;
                    }
                    this.maxPage = data.maxPage;
                    this.page += 1;
                }
            );
        }
    }

    reset() {
        this.flag = true;
        this.more = false;
        this.check = true;
        this.page = 0;
        this.maxPage = 1;
    }

    setSEO(data: Post, title: Title, meta: Meta) {
        title.setTitle(data.title);

        meta.updateTag({ name: 'description', content: data.excerpt });
        meta.updateTag({ name: 'url', content: this.router.url });
        meta.updateTag({ name: 'og:image', content: data.image });
        meta.updateTag({ name: 'twitter:image', content: data.image });
        meta.updateTag({ property: 'og:Title', content: data.title });
        meta.updateTag({ property: 'og:description', content: data.excerpt });
        meta.updateTag({ property: 'og:url', content: this.router.url });
        meta.updateTag({ property: 'og:type', content: 'website' });
        meta.updateTag({ property: 'twitter:title', content: data.title });
        meta.updateTag({ property: 'twitter:description', content: data.excerpt });
        meta.updateTag({ property: 'twitter:card', content: data.image });

        this.schema = {
            "@context": "http://schema.org",
            "@type": "Organization",
            "url": this.router.url,
            "name": data.title
        };
    }
}
