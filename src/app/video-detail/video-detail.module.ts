import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { NgxJsonLdModule } from "@ngx-lite/json-ld";

import { LoaderModule } from "../caller/loader/loader.module";
import { AppShellModule } from "../directives/appShell.module";
import { VideoDetailComponent } from "./video-detail.component";
import { VideoResolver } from "../caller/video.resolver"
import { ModalModule } from "../directives/modal.module";

const routes: Routes = [
    {
        path: ":slug",
        component: VideoDetailComponent,
        resolve: {
            video: VideoResolver
        }
    }
];

@NgModule({
    declarations: [VideoDetailComponent],
    imports: [CommonModule, RouterModule.forChild(routes), LoaderModule, NgxJsonLdModule, AppShellModule, ModalModule],
    providers: [VideoResolver]
})

export class VideoDetailModule { }
