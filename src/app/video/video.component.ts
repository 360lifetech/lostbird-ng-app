import { Title, Meta } from '@angular/platform-browser';
import { Component, OnInit, HostListener, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { HttpService } from '.././caller/http.service';
import { MenuService } from '../caller/menu.service';
import { LocalService } from '../caller/local.service';
import { NewPost, Cate } from '../globals';

@Component({
    selector: 'app-video',
    templateUrl: './video.component.html'
})

export class VideoComponent implements OnInit {
    link: string = '/video';
    headImage: string = 'http://placehold.it/720x340/fffdf4/333333?text=+';

    cate = new Cate;
    video: NewPost;
    first;
    news;
    menu: Object;

    page: number = 0;
    maxPage: number = 1;
    flag: boolean = true;
    more: boolean = false;

    schema: Object;

    private load: Subscription;
    private isBrowser;

    constructor(
        private singleService: HttpService,
        private menuService: MenuService,
        private localService: LocalService,
        private router: Router,
        private title: Title,
        private meta: Meta,
        @Inject(PLATFORM_ID) private platformId: object
    ) {
        this.isBrowser = isPlatformBrowser(this.platformId);
        if(this.isBrowser) {
            window.scrollTo(0, 0);
            this.localService.add(this.router.url, 'lostbird-history'); 
        }
    }

    @HostListener("window:scroll", []) onWindowScroll() {
        if (window.innerHeight + window.scrollY > window.innerHeight * 2) {
            if (window.innerHeight + window.scrollY >= document.querySelector('.wrapper').clientHeight - 400 && this.flag) {
                this.flag = false;
                this.loadMore();
            }
        }
    }

    loadMore() {
        if (this.page != this.maxPage) {
            this.singleService.API_ENDPOINT = '/videos/' + this.page;
            this.load = this.singleService.getNewPost().subscribe(
                data => {
                    this.video = data;
                    if (this.more) {
                        this.news = [].concat(this.news, this.video.posts);
                    } else {
                        this.first = this.video.posts;
                        this.news = this.video.posts;
                        this.news = this.news.splice(3);

                        this.headImage = this.first[0].image;
                        this.link = this.first[0].link;

                        this.more = true;
                    }
                    this.maxPage = data.maxPage;
                    this.page += 1;
                }
            );
        }
    }

    ngOnInit() {
        this.menu = this.menuService.getMenuChild(0)['child'];
        this.cate.name = 'Videos Archive - Lost Bird';
        this.setSEO(this.cate, this.title, this.meta);
        this.loadMore();
    }

    setSEO(data: Cate, title: Title, meta: Meta) {
        title.setTitle(data.name);

        meta.updateTag({ name: 'description', content: data.description });
        meta.updateTag({ name: 'url', content: this.router.url });
        meta.updateTag({ name: 'og:image', content: data.image });
        meta.updateTag({ name: 'twitter:image', content: data.image });
        meta.updateTag({ property: 'og:Title', content: data.name });
        meta.updateTag({ property: 'og:description', content: data.description });
        meta.updateTag({ property: 'og:url', content: this.router.url });
        meta.updateTag({ property: 'og:type', content: 'website' });
        meta.updateTag({ property: 'twitter:title', content: data.name });
        meta.updateTag({ property: 'twitter:description', content: data.description });
        meta.updateTag({ property: 'twitter:card', content: data.image });

        this.schema = {
            "@context": "http://schema.org",
            "@type": "Organization",
            "url": this.router.url,
            "name": data.name
        };
    }
}
