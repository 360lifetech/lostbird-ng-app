import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { NgxJsonLdModule } from "@ngx-lite/json-ld";

import { LoaderModule } from "../caller/loader/loader.module";
import { AppShellModule } from "../directives/appShell.module";
import { VideoComponent } from "./video.component";
import { ModalModule } from "../directives/modal.module";

const routes: Routes = [
    {
        path: "",
        component: VideoComponent,
    },
    {
        path: "",
        loadChildren: "../../app/video-detail/video-detail.module#VideoDetailModule",
    }
];

@NgModule({
    declarations: [VideoComponent],
    imports: [CommonModule, RouterModule.forChild(routes), LoaderModule, NgxJsonLdModule, AppShellModule, ModalModule]
})

export class VideoModule { }
